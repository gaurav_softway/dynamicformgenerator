//
//  SWFormViewController.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 18/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

class SWFormSubmitVC: UIViewController, SWFormGeneratorDelegate {
    var data = NSMutableData()
    var formMetaData: Dictionary<String,AnyObject>!
    var recievedData : NSMutableData?
    var formManager : SWFormManager?
    override func viewDidLoad() {
        
    }
    
    //MARK:- action methods
    @IBAction func handleShowSubmissionForm(sender: AnyObject) {
        self.formManager = SWFormManager()
        let formTVC: SWFormViewController = SWFormViewController.initWith("http://www.json-generator.com/api/json/get/cagMrjRbGW",formID: "234", stryboardName: STORYBOARDNAME, viewControllerIdentifier: VIEWCONTROLLERID) as! SWFormViewController
        formTVC.delegate = self
        formTVC.formManager = self.formManager
        self.navigationController?.pushViewController(formTVC, animated: true)
        
    }
    //MARK:- SWBaseFormGeneratorDelegate methodes
    func formGenerator( formGenerator:SWBaseFormGenerator, didCancelJSONwith resultJSON: AnyObject)
    {
        println("\(__FUNCTION__)")
    }
    
    
    func formGenerator( formGenerator:SWBaseFormGenerator, shouldSubmitJSON jsonData: AnyObject, withURL:String) -> Bool
    {
        println("\(__FUNCTION__)")
        return true
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, willSubmitJSON jsonData: AnyObject, withURL:String)
    {
        
        println("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, didSubmitJSONwith resultJSON: AnyObject, withURL:String)
    {
        println("\(__FUNCTION__)")
        println(resultJSON.description)
    }
    
    
    func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingJSONWith jsonData: AnyObject)
    {
        println("\(__FUNCTION__)")
        
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingURLWith URL: String)
    {
        println("\(__FUNCTION__)")
        
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, doneJSONLoadWith jsonData: AnyObject)
    {
        println("\(__FUNCTION__)")
        
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, startedFormRenderingWith jsonData: AnyObject)
    {
        println("\(__FUNCTION__)")
        
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, doneFormRenderingWith jsonData: AnyObject)
    {
        println("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, isPreparingForNextwithLoadingURL URL: String)
    {
        println("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, isDoneWithResultJSON JSON: AnyObject)
    {
        println("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, buttonTappedWith button : UIButton, withFieldId id: String)
    {
        println("\(__FUNCTION__)")
    }
}
