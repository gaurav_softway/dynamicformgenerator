//
//  ViewController.swift
//  DynamicFormGenerator
//
//  Created by Gaurav Keshre on 6/16/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit



class SWFormDoneVC: UIViewController, SWFormGeneratorDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    var data = NSMutableData()
    var formMetaData: Dictionary<String,AnyObject>!
    var recievedData : NSMutableData?
    var formManager : SWFormManager?
    override func viewDidLoad() {
        
    }
   
    //MARK:- action methods
    @IBAction func showDoneForm(sender: AnyObject) {
        self.formManager = SWFormManager()
        let formTVC: SWFormViewController = SWFormViewController.initWith("http://www.json-generator.com/api/json/get/cqvgZEQCiG",formID: "6788", stryboardName: STORYBOARDNAME, viewControllerIdentifier: VIEWCONTROLLERID) as! SWFormViewController
        formTVC.delegate = self
        formTVC.formManager = self.formManager
        self.navigationController?.pushViewController(formTVC, animated: true)
    }

    
    //MARK:- SWBaseFormGeneratorDelegate methodes
    func formGenerator( formGenerator:SWBaseFormGenerator, didCancelJSONwith resultJSON: AnyObject)
    {
        println("\(__FUNCTION__)")
    }
    
    
    func formGenerator( formGenerator:SWBaseFormGenerator, shouldSubmitJSON jsonData: AnyObject, withURL:String) -> Bool
    {
        println("\(__FUNCTION__)")
        return true
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, willSubmitJSON jsonData: AnyObject, withURL:String)
    {
        
        println("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, didSubmitJSONwith resultJSON: AnyObject, withURL:String)
    {
        println("\(__FUNCTION__)")
        
    }
    
    
    func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingJSONWith jsonData: AnyObject)
    {
        println("\(__FUNCTION__)")
        
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingURLWith URL: String)
    {
        println("\(__FUNCTION__)")
        
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, doneJSONLoadWith jsonData: AnyObject)
    {
        println("\(__FUNCTION__)")
        
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, startedFormRenderingWith jsonData: AnyObject)
    {
        println("\(__FUNCTION__)")
        
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, doneFormRenderingWith jsonData: AnyObject)
    {
        println("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, isPreparingForNextwithLoadingURL URL: String)
    {
        println("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, isDoneWithResultJSON JSON: AnyObject)
    {
        self.textView.text = JSON.description
        self.imageView.image = JSON["user_image"] as? UIImage
        
        println("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, buttonTappedWith button : UIButton, withFieldId id: String)
    {
        println("\(__FUNCTION__)")
    }
}


