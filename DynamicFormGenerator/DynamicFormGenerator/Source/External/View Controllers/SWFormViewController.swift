//
//  SWFormViewController.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 07/07/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

class SWFormViewController: SWBaseFormGenerator {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
}
