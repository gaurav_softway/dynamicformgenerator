//
//  NSUrl+Additions.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 10/07/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

extension NSURL{
    func doNotBackUp() 
    {
        var error : NSError?
        self.setResourceValue(true, forKey: NSURLIsExcludedFromBackupKey, error: &error)
    }
    
}
