//
//  UIFont+Additions.swift
//  DynamicFormGenerator
//
//  Created by Gaurav Keshre on 7/2/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
import UIKit

enum TextClass : String{
    case H1 = "H1"
    case H2 = "H2"
    case H3 = "H3"
    case H4 = "H4"
    case H5 = "H5"
    case P  = "P"
}

enum SWHTMLText{
    case Default
    case Bold
    case Normal
}

extension UIFont {
    
    convenience init(styleTag: String, var fontFamily: String?){
        if fontFamily == nil{
            fontFamily = "Helvetica" //default font
        }
        let textStyle:TextClass = TextClass(rawValue: styleTag.uppercaseString)!

        var strTextStyle: String

        switch textStyle{
        case .H1:
            strTextStyle = UIFontTextStyleHeadline
        case .H2:
            strTextStyle = UIFontTextStyleSubheadline
        case .H3:
            strTextStyle = UIFontTextStyleCaption1
        case .H4:
            strTextStyle = UIFontTextStyleCaption2
        case .H5:
            strTextStyle = UIFontTextStyleFootnote
        case .P:
            strTextStyle = UIFontTextStyleBody
        default:
            strTextStyle = UIFontTextStyleBody
        }
        
        let fontDescriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(strTextStyle)
        
        self.init(descriptor:fontDescriptor, size: fontDescriptor.pointSize)
    }

    convenience init?(var fontFamily: String!, bold: Bool,withFontSize size: CGFloat = 12) {
        if fontFamily == nil{
            fontFamily = "Helvetica" //default font
        }
        if bold{
            fontFamily = fontFamily + "-Bold"
        }
        self.init(name: fontFamily!, size: size)
    }
    
}



