//
//  SWFormManager.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 22/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

class SWFormManager: NSObject {

    lazy var arrFormData = [AnyObject]()
    
    func addForm(form_id: String, withFormData form_data: AnyObject){
    var recordForm = [form_id: form_data] as Dictionary
    self.arrFormData.append(recordForm)
    }
    
    func dataForFormID(form_id: String) -> AnyObject{
        
        var returnData: Dictionary<String, AnyObject> =  Dictionary<String, AnyObject>()
        
        (arrFormData as! [Dictionary<String, AnyObject>]).filter { (data:Dictionary) -> Bool in
            if let vd: AnyObject = data[form_id]{
                returnData = vd as! Dictionary<String, AnyObject>
                return true;
            }
            return false
        }
        return returnData
    }
}
