//
//  SWJSONFileManager.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 10/07/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

class SWJSONFileManager: NSObject {
    
    class var sharedInstance : SWJSONFileManager {
        struct Static {
            static var instance : SWJSONFileManager?
            static var token : dispatch_once_t = 0
        }
        dispatch_once(&Static.token) {
            Static.instance = SWJSONFileManager()
        }
        return Static.instance!
    }

    func saveJSON( json: AnyObject, forFormId  id : String) -> String
    {
        var error: NSError?
        
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsDirectory: AnyObject = paths[0]
        let fileManager = NSFileManager.defaultManager()
        let dataPath = documentsDirectory.stringByAppendingPathComponent("SampleJsonFiles")
        if (!fileManager.fileExistsAtPath(dataPath)) {
            NSFileManager.defaultManager() .createDirectoryAtPath(dataPath, withIntermediateDirectories: false, attributes: nil, error: &error)
             NSURL(fileURLWithPath: dataPath)?.doNotBackUp()
        }
        if let jsonData = NSJSONSerialization.dataWithJSONObject(json, options: nil, error: &error)
        {
            var jsonFilePath = dataPath.stringByAppendingPathComponent("\(id).png")
            if (fileManager.fileExistsAtPath(jsonFilePath))
            {
                fileManager.removeItemAtPath(jsonFilePath, error: &error)
                
            }
          fileManager.createFileAtPath(jsonFilePath, contents: jsonData, attributes: nil)
            return jsonFilePath
        }
        return ""
    }
    func retriveJSON(forFormId  id : String) -> AnyObject
    {
        
        
        var paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsDirectory: AnyObject = paths[0]
        let fileManager = NSFileManager.defaultManager()
        let dataPath = documentsDirectory.stringByAppendingPathComponent("SampleJsonFiles")
        
       
        
        if (!fileManager.fileExistsAtPath(dataPath)) {
            return []
        }
        
        
        var jsonFilePath = dataPath.stringByAppendingPathComponent("\(id).png")
        if (!fileManager.fileExistsAtPath(jsonFilePath))
        {
            return []
        }

        
        if let jsonMetaData = NSData(contentsOfURL: NSURL(fileURLWithPath: jsonFilePath)!)
        {
            var error: NSError?
            return NSJSONSerialization.JSONObjectWithData(jsonMetaData, options: nil, error: &error)!
        }
        return []
    }
}
