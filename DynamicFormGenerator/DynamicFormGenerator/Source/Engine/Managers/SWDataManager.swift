//
//  SWDataManager.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 01/07/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit
import Foundation
import CoreData

let LAST_MODIFIED_DATE_ON_SERVICE = "Date"
let DATE_FORMAT = "EEE, dd MM yyyy HH:mm:ss zzz"
typealias SWDataManagerCompletionBlock = (Bool, AnyObject?) -> ()

class SWDataManager: NSObject, NSURLConnectionDelegate {
    
    var responseBlock: SWDataManagerCompletionBlock?
    var completionBlock: SWDataManagerCompletionBlock?
    var formId: String?
    let queue : NSOperationQueue = NSOperationQueue()
    var responseData = NSMutableData()
    var lastModificationDate : NSDate = NSDate()
    
    
    class var sharedInstance : SWDataManager {
        struct Static {
            static var instance : SWDataManager?
            static var token : dispatch_once_t = 0
        }
        dispatch_once(&Static.token) {
            Static.instance = SWDataManager()
        }
        return Static.instance!
    }
    
    func getJSONForFormId(id: String?, withURL url: String?, onCompletion completionBlock: SWDataManagerCompletionBlock?){
        self.completionBlock = completionBlock
        self.formId = id
        self.createConnection(URL: url!, withResponseCallBack: { (status:Bool, response:AnyObject?) -> () in
            self.completionBlock!(status,response)
        })
    }
    
    func createConnection(URL url: String, withResponseCallBack responseCallBack: SWDataManagerCompletionBlock)
    {
        self.responseData = NSMutableData()
        self.responseBlock = responseCallBack
        let URL = NSURL(string:url)
        let request = NSURLRequest(URL: URL!)
        var error: NSError?
        var connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: true)!
        connection.start()
        
    }
    
    //MARK: - NSURLConnectionDelegate methods
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        
        // Check the last modified date on service and local last modified date, if local last modified date is smaller then last modified date on servie then download the data else cancel the connection and return the local data on response block
        
        var lastModifiedDateOnService: NSDate = NSDate()
        
        //Notice the OPTIONAL BINDING on multiple variable in single expression
        if let httpResponse = response as? NSHTTPURLResponse, let dateString = httpResponse.allHeaderFields[LAST_MODIFIED_DATE_ON_SERVICE] as? String{
            if let dateString = httpResponse.allHeaderFields[LAST_MODIFIED_DATE_ON_SERVICE] as? String {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = DATE_FORMAT
                lastModifiedDateOnService = dateFormatter.dateFromString(dateString)!
                dateFormatter.timeZone = NSTimeZone.localTimeZone()
                println(lastModifiedDateOnService)
                lastModificationDate = lastModifiedDateOnService
                
                
                let predicate = NSPredicate(format: "id ==[c] %@", formId!)
                let formObj: AnyObject? =  CoreDataManager.searchObjectsForEntity("SWFormMO", withPredicate: predicate, andSortKey: nil, andSortAscending: true).firstObject
                
                if formObj != nil
                {
                    var date = dateFormatter.stringFromDate((formObj as! SWFormMO).last_modified_date)
                    
                    let newModifiedDateOnService = dateFormatter.dateFromString(date)!
                    dateFormatter.timeZone = NSTimeZone.localTimeZone()
                    println(newModifiedDateOnService)
                    
                    if (formObj as! SWFormMO).last_modified_date.compare(lastModifiedDateOnService) != NSComparisonResult.OrderedAscending
                    {
                        let jsonData = SWJSONFileManager.sharedInstance.retriveJSON(forFormId: self.formId!) as! Dictionary<String, AnyObject>
                        self.responseBlock!(true, jsonData)
                        connection.cancel()
                        
                    }
                    else
                    {
                        //delete the object in core data
                        
                        CoreDataManager.deleteAllObjectsForEntity("SWFormMO", withPredicate: predicate, andContext: CoreDataManager.mainManagedObjectContext())
                        var err: NSError?
                        if !CoreDataManager.saveWithError(&err)
                        {
                            println(err)
                        }
                    }
                    
                }
            }
        }
    }
    func connection(connection: NSURLConnection, didSendBodyData bytesWritten: Int, totalBytesWritten: Int, totalBytesExpectedToWrite: Int) {
        var err: NSError?
        var headerData = NSJSONSerialization.JSONObjectWithData(self.responseData, options: NSJSONReadingOptions.AllowFragments, error: &err) as! Dictionary<String,AnyObject>
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!){
        self.responseData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!)
    {
        var err: NSError?
        if self.responseData.length > 0
        {
            var formMetaData = Dictionary<String,AnyObject>()
            
            if let tempFormMetaData = NSJSONSerialization.JSONObjectWithData(self.responseData, options: NSJSONReadingOptions.AllowFragments, error: &err) as? Dictionary<String,AnyObject> {
                formMetaData = tempFormMetaData
            }
            if err != nil
            {
                self.responseBlock!(false, err)
            }
            else
            {
                self.responseBlock!(true, formMetaData)
                
                let jsonFilePath = SWJSONFileManager.sharedInstance.saveJSON(formMetaData, forFormId: self.formId!)
                
                
                let formMO =  SWFormMO.insertInManagedObjectContext(CoreDataManager.mainManagedObjectContext())
                formMO.id = self.formId!
                formMO.last_modified_date = self.lastModificationDate
                formMO.checksum = "Check Sum"
                formMO.path = jsonFilePath
                
                var err: NSError?
                if !CoreDataManager.saveWithError(&err)
                {
                    println(err)
                }
            }
        }
        else
        {
            self.responseBlock!(false, err)
        }
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError)
    {
        if error.code == -1009
        {
            let predicate = NSPredicate(format: "id ==[c] %@", formId!)
            let formObj: AnyObject? =  CoreDataManager.searchObjectsForEntity("SWFormMO", withPredicate: predicate, andSortKey: nil, andSortAscending: true).firstObject
            
            if formObj != nil
            {
                let jsonData = SWJSONFileManager.sharedInstance.retriveJSON(forFormId: self.formId!) as! Dictionary<String, AnyObject>
                self.responseBlock!(true, jsonData)
                connection.cancel()
            }
        }
    }
}
