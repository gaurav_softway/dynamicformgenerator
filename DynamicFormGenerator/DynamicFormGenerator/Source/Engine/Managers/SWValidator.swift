//
//  SWValidator.swift
//  DynamicFormGenerator
//
//  Created by Gaurav Keshre on 6/16/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation


let rgxName = "\\w.*"; //"[A-Za-z]+[0-9A-Za-z]*";//

let rgxUsername: String =  "[a-zA-Z]+[0-9a-zA-Z]*[_ .]{0,1}[0-9a-zA-Z]+"
;
let rgxUsername_2 = "[A-Za-z]{1,}[A-Za-z_.0-9]*[A-Za-z0-9]{1,}";
//[A-Za-z]+[A-Za-z_.0-9]*[A-Za-z0-9]+ -> equivalent to above one

let rgxEmailID = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";

let rgxUSPhoneNumber = "[(][0-9]{3}[)] [0-9]{3}[-][0-9]{4}"; //gk

let regexURL = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";

let rgxURL   = "((http|https)://)*((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";

let rgxBirthdateMonth = "^(0?[1-9]|[12]\\d|3[01]) \\b(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May?|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sept(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)$";

let rgxMonth = "(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May?|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sept(?:ember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?)$";


enum ValidatorRegex: Int{
    case Name = 0, Email, Username_1, Username_2, Phone, NumericOnly, AlphabetOnly, AlphaNumeric, FlaotOnly, BeginsWithNumber, BeginsWithAlphabet, Month
    
    func description()->String{
        switch self{
        case .Email :
            return rgxEmailID
        case .Username_1 :
            return rgxUsername
        case .Username_2 :
            return rgxUsername_2
        case .Phone :
            return rgxUSPhoneNumber
        case .NumericOnly :
            return "^[0-9]+$"
        case .AlphabetOnly:
            return "^[a-zA-Z ]+$"
        case .AlphaNumeric:
            return "^[0-9a-zA-Z ]+$"
        case .FlaotOnly:
            return "^[0-9.]+$"
        case .BeginsWithAlphabet:
            return "^[a-zA-Z]+[0-9]*$"
        case .BeginsWithNumber:
            return "^[0-9]+[a-zA-Z]*$"
        case .Month:
            return rgxMonth
        case .Name:
            return rgxName
        }
    }
}


enum ValidatorOutput{
    case Valid, Invalid, Underflow, Overflow
    
}

typealias SWValidationCompletionBlock = (Bool, String?) -> ()
typealias SWValidationFailureBlock = (Bool, [Dictionary<String, String>]) -> ()

class SWValidator {
    func validate(stringToTest: String, againstValidator: ValidatorRegex, customRegex: String!)-> ValidatorOutput{
        var op: ValidatorOutput = .Valid
        return op
    }
    
    
    //--------------------------------------
    // MARK: - Convenience
    //--------------------------------------
    
    class func removePhoneNumberFormattingFromString (var str: String)-> String{
        
        str =  str.stringByReplacingOccurrencesOfString("(", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil);
        
        str =  str.stringByReplacingOccurrencesOfString(")", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil);
        
        str =  str.stringByReplacingOccurrencesOfString("+", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil);
        
        str =  str.stringByReplacingOccurrencesOfString("-", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil);
        
        str =  str.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.CaseInsensitiveSearch, range: nil);
        return str
    }
    
    func validateForForm(formInfo:SWFormVO?, validationCompletionBlock:SWValidationCompletionBlock, onFailure failureBlock: SWValidationFailureBlock)
    {
        var status  : Bool = true
        var statusMessage : String?
        var invalidIds  : [Dictionary<String, String>]?
        
        for fieldInfo in formInfo!.fields as [SWFieldVO]
        {
            if fieldInfo.needsValidation
            {
                if fieldInfo.resultDict == nil
                {
                    status = false
                    statusMessage = fieldInfo.validationInfo!["error_message"] as? String
                    if invalidIds == nil
                    {
                        invalidIds = [Dictionary<String, String>]()
                    }
                    invalidIds?.append([fieldInfo.id : statusMessage! as String])
                }
                else
                {
                    switch(fieldInfo.fieldType)
                    {
                    case SWFormFieldType.TextField:
                        fallthrough
                    case SWFormFieldType.TextView:
                        if self.validateString((fieldInfo.resultDict![fieldInfo.fieldKey] as? String)!, with: (fieldInfo.validationInfo!["regex"] as? String)!) == false
                        {
                            status = false
                            statusMessage = fieldInfo.validationInfo!["error_message"] as? String
                            if invalidIds == nil
                            {
                                invalidIds = [Dictionary<String, String>]()
                            }
                            invalidIds?.append([fieldInfo.id : statusMessage! as String])
                        }
                    default:
                        println()
                    }
                }
            }
            
        }
        if status == true
        {
            validationCompletionBlock(status, statusMessage)
        }
        else
        {
            failureBlock(status, invalidIds!)
        }
    }
    
    func validateString(string: String , with regex:String) -> Bool
    {
        let matchPredicate = NSPredicate(format:"SELF MATCHES %@", regex)
        return matchPredicate.evaluateWithObject(string)
    }
    
}