//
//  SWBaseVO.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 17/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
import UIKit

enum SWFormAction: String {
    case None       = ""
    case Submit     = "submit"
    case Cancel     = "cancel"
    case Next       = "next"
    case Done       = "done"
}

//MARK:- SWFormFieldType
enum SWFormFieldType : String {
    case None              = "UNKNOWN"
    case ActivityIndicator = "SWActivityIndicatorCell"
    case Button            = "SWButtonCell"
    case RadioButton       = "SWRadioButtonCell"
    case CheckBox          = "SWCheckBoxCell"
    case TextField         = "SWTextFieldCell"
    case TextView          = "SWTextViewCell"
    case ImagePicker       = "SWImagePickerCell"
    case SegmentControl    = "SWSegmentCell"
    case Switch            = "SWSwitchCell"
    case Slider            = "SWSliderCell"
    case Stepper           = "SWStepperCell"
}


//MARK:- enumFromStringFieldType
func enumFromStringFieldType(value:String)-> SWFormFieldType
{
    
    switch value {
        // Use Internationalization, as appropriate.
    case "SWActivityIndicatorCell":
        return .ActivityIndicator
    case "SWButtonCell":
        return .Button
    case "SWRadioButtonCell":
        return .RadioButton
    case "SWCheckBoxCell":
        return .CheckBox
    case "SWTextFieldCell":
        return .TextField
    case "SWTextViewCell":
        return .TextView
    case "SWImagePickerCell":
        return .ImagePicker
    case "SWSegmentCell":
        return .SegmentControl
    case "SWSwitchCell":
        return .Switch
    case "SWSliderCell":
        return .Slider
    case "SWStepperCell":
        return .Stepper
    default:
        return .None
    }
}

//MARK:- SWBaseVO
class SWBaseVO {
    let id : String
    var fallbackInfo : Dictionary<String,AnyObject>?
    var userValues: String?
    init(_id: String, _fallBackInfo: Dictionary<String,AnyObject>)
    {
        self.fallbackInfo = _fallBackInfo
        self.id = _id
    }
    init(_id: String)
    {
        self.fallbackInfo = nil
        self.id = _id
    }
    func createFallbackWith(baseURL: String,fallbackString: String) -> String
    {
        return baseURL.stringByAppendingString(fallbackString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
    }
}

