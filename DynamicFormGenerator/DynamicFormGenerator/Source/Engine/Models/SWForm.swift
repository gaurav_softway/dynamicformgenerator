//
//  SWFormVO.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 18/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
//MARK:- SWFormVO
class SWFormVO: SWBaseVO {
    var fields : [SWFieldVO] = []
    let navigationInfo : Dictionary<String,AnyObject>?
    
    init(jsonData:Dictionary<String,AnyObject>, withID formID: String)
    {
        self.navigationInfo = (jsonData["navigation_info"] as? Dictionary<String,AnyObject>) ?? nil
        
        super.init(_id: formID, _fallBackInfo: ((jsonData["fallback_info"] as? Dictionary<String,AnyObject>) ?? nil)!)
        let arrFields : [AnyObject] = (jsonData["fields"] as? Array)!
        for dataDict in arrFields
        {
            let formField :SWFieldVO = SWFieldVO(fieldData: dataDict as! Dictionary<String,AnyObject>)
            self.fields.append(formField)
        }
    }
    func createFallbackWith(baseURL: String) -> String
    {
        var URLString = self.navigationInfo?["url"] as! String
        return baseURL.stringByAppendingString(URLString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
    }

    //TODO:- Need to complete this implementation
    func getNavivationURL() -> String{
        
        let s1 = self.navigationInfo?["url"] as! String
        let s2 = self.fallbackInfo?["url"] as! String
        
        
        return s2 + s1
    }

}
