//
//  SWFormMO.swift
//  DynamicFormGenerator
//
//  Created by Sugeet on 06/07/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
import CoreData
@objc(SWFormMO)

class SWFormMO: NSManagedObject {

    @NSManaged var id: String
    @NSManaged var last_modified_date: NSDate
    @NSManaged var path: String
    @NSManaged var checksum: String
   
    //MARK: - Insert SWFormMO Data
    class func insertInManagedObjectContext(moContext: NSManagedObjectContext?) -> SWFormMO  {
        var form =  NSEntityDescription.insertNewObjectForEntityForName("SWFormMO", inManagedObjectContext: moContext!) as! SWFormMO
        form.id = ""
        form.last_modified_date = NSDate()
        form.checksum = ""
        form.path = ""
        return form
    }
}
