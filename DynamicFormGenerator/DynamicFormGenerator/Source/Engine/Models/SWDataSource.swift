//
//  SWDataSource.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit
typealias SWDataSourceCellConfigurBlock = (UITableViewCell,NSIndexPath) -> ()

class SWDataSource: NSObject , UITableViewDataSource {
    var tableView : UITableView!
    var viewController : SWBaseFormGenerator?
    var dictData : Dictionary<String,AnyObject>!
    var formMetaData : SWFormVO?
    var shouldAllowCellconfiguration : Bool?
    var cellConfigurationBlock : SWDataSourceCellConfigurBlock?
    var composedResultDict: Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
    var currentOptionIndex = -1
    
    //--------------------------------------
    // MARK: - Data
    //--------------------------------------

    func reloadDataWith(dataDict: Dictionary<String,AnyObject>, withCellConfigureBlock configureBlock: SWDataSourceCellConfigurBlock?, withTableView _tableView: UITableView, _viewController: SWBaseFormGenerator?, withId formId : String)
    {
        self.tableView = _tableView
        self.dictData =  dataDict
        self.cellConfigurationBlock = configureBlock
        self.dictData = dataDict
        self.formMetaData = SWFormVO(jsonData: self.dictData["form"] as! Dictionary<String,AnyObject>, withID: formId)
        self.tableView!.reloadData()
        self.viewController = _viewController
    }

    //--------------------------------------
    // MARK: - TableViewDataSource
    //--------------------------------------

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count : Int = (self.formMetaData?.fields.count)!
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let field: SWFieldVO = self[indexPath.row]!
        
        let cell : SWBaseFieldCell = tableView.dequeueReusableCellWithIdentifier(field.fieldType.rawValue,
            forIndexPath: indexPath) as! SWBaseFieldCell

        cell.setDelegate(field.fieldType, delegate: self.viewController!)
        cell.configureCellWithMetaData(field)
        if let configBlock = self.cellConfigurationBlock{
            configBlock(cell,indexPath)
        }

        return cell
    }
    

    //--------------------------------------
    // MARK: - SubScripting (CLANG3.1) methods
    //--------------------------------------

    subscript(index: Int) -> SWFieldVO? {
        return self.formMetaData?.fields[index]
    }
    
    subscript(navigation key: String) -> String? {
        return  (self.formMetaData?.navigationInfo![key] as? String)?.lowercaseString
    }

}

