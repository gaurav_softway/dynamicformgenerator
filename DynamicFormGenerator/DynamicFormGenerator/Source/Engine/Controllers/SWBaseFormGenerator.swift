//
//  SWBaseFormGenerator.swift
//  DynamicFormGenerator
//
//  Created by Gaurav Keshre on 6/16/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
import UIKit

enum SWBaseFormGeneratorSourceType : Int
{
    case NONE
    case JSON
    case URL
    case LOCAL_FILE
}

@objc protocol SWFormGeneratorCancelDelegate{
//    func formGenerator( formGenerator:SWBaseFormGenerator, shouldCancelJSON jsonData: Dictionary<String,AnyObject>, withURL:String) -> Bool
//    func formGenerator( formGenerator:SWBaseFormGenerator, willCancelJSON jsonData: Dictionary<String,AnyObject>, withURL:String)
    func formGenerator( formGenerator:SWBaseFormGenerator, didCancelJSONwith resultJSON: AnyObject)
}

@objc protocol SWFormGeneratorSubmitDelegate{
    func formGenerator( formGenerator:SWBaseFormGenerator, shouldSubmitJSON jsonData: AnyObject, withURL:String) -> Bool
    func formGenerator( formGenerator:SWBaseFormGenerator, willSubmitJSON jsonData: AnyObject, withURL:String)
    func formGenerator( formGenerator:SWBaseFormGenerator, didSubmitJSONwith resultJSON: AnyObject, withURL:String)
}

@objc protocol SWFormGeneratorDelegate: SWFormGeneratorSubmitDelegate, SWFormGeneratorCancelDelegate{
    
    optional func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingJSONWith jsonData: AnyObject)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingURLWith URL: String)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, doneJSONLoadWith jsonData: AnyObject)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, startedFormRenderingWith jsonData: AnyObject)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, doneFormRenderingWith jsonData: AnyObject)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, isPreparingForNextwithLoadingURL URL: String)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, isDoneWithResultJSON JSON: AnyObject)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, buttonTappedWith button : UIButton, withFieldId id: String)
}



class SWBaseFormGenerator: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SWButtonCellDelegte, SWImagePickerCellDelegate,SWSegmentCellDelegate,SWSliderCellDelegate,SWStepperCellDelegate,SWSwitchCellDelegate, SWTextFieldCellDataChangeDelegate , SWTextViewCellDataChangeDelegate, SWOptionTableViewControllerDelegate{
    
    
    weak var delegate: SWFormGeneratorDelegate?
    var showAlternateColors: Bool? = false
    var formMetaData: Dictionary<String,AnyObject>!
    var formMetaDataURL: String!
    var formId: String!
    
    var imagePicker :UIImagePickerController?
    var imagePicCompletionBlock:SWImagePickCompletionBlock?
    var validationCompleteBlock:SWValidationCompletionBlock?
    
    var imagePickIndexPath : NSIndexPath?
    var generatorType: SWBaseFormGeneratorSourceType = .NONE
    
    var arrAlternateColors: Array<UIColor> = []
    //--------------------------------------
    // MARK: - Lazy Properties
    //--------------------------------------
    
    lazy var validator : SWValidator = SWValidator()
    
    lazy var dataSource : SWDataSource = SWDataSource()
    
    @IBOutlet weak var btnAction: UIBarButtonItem!
    weak var formManager: SWFormManager?
    
    // MARK:- Init Methods
    class func initWith(URL: String, formID: String, stryboardName:String, viewControllerIdentifier:String) -> AnyObject? {
        let storyboard = UIStoryboard(name: stryboardName, bundle: NSBundle(forClass: SWBaseFormGenerator.self))
        if let controller = storyboard.instantiateViewControllerWithIdentifier(viewControllerIdentifier) as? SWBaseFormGenerator
        {
            controller.generatorType = SWBaseFormGeneratorSourceType.URL
            controller.formMetaDataURL = URL
            controller.formId = formID
            return controller
        }
        return nil
    }
    
    class func initWithformMetaData(formMetaDataDict: Dictionary<String,AnyObject>, formID: String, stryboardName:String, viewControllerIdentifier:String) -> AnyObject? {
        let storyboard = UIStoryboard(name: stryboardName, bundle: NSBundle(forClass: SWBaseFormGenerator.self))
        if let controller = storyboard.instantiateViewControllerWithIdentifier(viewControllerIdentifier) as? SWBaseFormGenerator
        {
            controller.generatorType = SWBaseFormGeneratorSourceType.JSON
            controller.formMetaData = formMetaDataDict
            controller.formId = formID
            return controller
        }
        return nil
    }
    class func initWithLocalURL(URL: String, stryboardName:String,  formID: String,viewControllerIdentifier:String) -> AnyObject? {
        let storyboard = UIStoryboard(name: stryboardName, bundle: NSBundle(forClass: SWBaseFormGenerator.self))
        if let controller = storyboard.instantiateViewControllerWithIdentifier(viewControllerIdentifier) as? SWBaseFormGenerator
        {
            controller.generatorType = SWBaseFormGeneratorSourceType.LOCAL_FILE
            controller.formMetaDataURL = URL
            controller.formId = formID
            return controller
        }
        return nil
    }
    
    // MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        switch(self.generatorType)
        {
        case SWBaseFormGeneratorSourceType.NONE :
            println()
        case SWBaseFormGeneratorSourceType.JSON :
            self.loadFormMetaData(self.formMetaData)
            println()
        case SWBaseFormGeneratorSourceType.URL :
            println()
            self.loadFormWith(self.formMetaDataURL)
        case SWBaseFormGeneratorSourceType.LOCAL_FILE :
            println()
            self.loadFormWithLocalpath(self.formMetaDataURL)
        default:
            println()
            
        }
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    // MARK:- Convinience Methods
    func loadFormWith(URL: String)
    {
        if self.delegate != nil
        {
            self.delegate?.formGenerator!(self, startedLoadingURLWith: URL)
        }

        SWDataManager.sharedInstance.getJSONForFormId("123", withURL:URL) { (status: Bool, response: AnyObject?) -> () in
            if status
            {
                dispatch_async(dispatch_get_main_queue(),{
                    self.loadFormMetaData(response as! Dictionary)
                })
            }
            else
            {
                //Handle error
                self.delegate?.formGenerator!(self, doneJSONLoadWith: self.formMetaData)
                self.navigationController?.popToRootViewControllerAnimated(true)
            }

        }
        
        
       
    }
    func loadFormWithLocalpath(path: String)
    {
        if self.delegate != nil
        {
            self.delegate?.formGenerator!(self, startedLoadingURLWith: path)
        }
        
        if NSFileManager.defaultManager().fileExistsAtPath(path)
        {
            var err: NSError?
            var data : NSData = NSFileManager.defaultManager().contentsAtPath(path)!
            var _formMetaData = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &err) as! Dictionary<String,AnyObject>
            if err != nil
            {
                self.loadFormMetaData(_formMetaData)
            }
            else
            {
                //Error
            }
        }
        else
        {
            //File does not exists
        }
        
    }
    
    func loadFormMetaData(dataDict: Dictionary<String,AnyObject>)
    {
        self.formMetaData = dataDict
        if self.delegate != nil && self.formMetaData != nil
        {
            self.delegate?.formGenerator!(self, startedLoadingJSONWith: self.formMetaData)
        }
        
        self.tableView.dataSource = self.dataSource
        
        if self.delegate != nil{
            self.delegate?.formGenerator!(self, doneJSONLoadWith: dataDict)
        }
        if self.delegate != nil && self.formMetaData != nil{
            
            self.delegate?.formGenerator!(self, startedFormRenderingWith: self.formMetaData)
            
            self.dataSource.reloadDataWith(self.formMetaData, withCellConfigureBlock: {
                (cell :UITableViewCell, indexPath:NSIndexPath) -> () in
                }, withTableView: self.tableView,_viewController: self, withId: self.formId)
        }
        
        if self.dataSource.formMetaData?.navigationInfo != nil{
            self.btnAction.title = self.dataSource[navigation: "button_title"]
        }
        
        if self.delegate != nil && self.formMetaData != nil{
            self.delegate?.formGenerator!(self, doneFormRenderingWith: self.formMetaData)
        }
        
    }
    
    //--------------------------------------
    // MARK: - UITableViewDelegate
    //--------------------------------------
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let attrib = self.dataSource[indexPath.row]!.attributesInfo{
            var cellHeight = attrib["cell_height"] as! CGFloat
            return cellHeight
        }
        return 64
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath)
        if let identifier = SWFormFieldType(rawValue: (tableView.cellForRowAtIndexPath(indexPath)!.reuseIdentifier)!){
            
            let objRBTVC = self.storyboard?.instantiateViewControllerWithIdentifier("SWOptionTableViewController") as! SWOptionTableViewController
            
            let _fieldInfo = self.dataSource.formMetaData!.fields[indexPath.row] //old
            
            let fieldInfo = self.dataSource[indexPath.row] //with subscripting
            
            objRBTVC.fieldInfo = fieldInfo
            
            let finalJSON = self.dataSource.composedResultDict
            
            objRBTVC.delegate = self
            
            switch identifier{
            case .RadioButton:
                objRBTVC.navTitle = "Choose your Option"
                objRBTVC.isCheckBoxType = false
                
            case .CheckBox:
                objRBTVC.navTitle = "Select Options"
                objRBTVC.isCheckBoxType = true
                
            default:
                print("ignore the did select")
                return
                //do nothing
            }
            self.navigationController?.pushViewController(objRBTVC, animated: true)
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if arrAlternateColors.count < 1 {
            return;
        }
        let index: Int = Int(indexPath.row % self.arrAlternateColors.count)
        let color = (arrAlternateColors[index]) as UIColor?
                cell.backgroundView?.backgroundColor = color
                cell.contentView.backgroundColor = color
    }
    
    //--------------------------------------
    // MARK: - Action Methods
    //--------------------------------------
    
    @IBAction func handleAction(sender: AnyObject) {
         let action = SWFormAction(rawValue: self.dataSource[navigation: "button_title"]!)!
        if action == SWFormAction.Cancel
        {
            self.actionFormCancel()
        }
        
        
        let urlNavigate = self.dataSource.formMetaData?.getNavivationURL()
        
        //--------------------------------------
        // MARK: - ValidateData
        //--------------------------------------
    
        self.validator.validateForForm(self.dataSource.formMetaData, validationCompletionBlock: { (status: Bool, msg:String?) -> () in
            if status{
                
                switch action{
                case .None:
                    print("Do Nothing")
                    return
                    
                case .Next:
                    print("Do Next")
                    self.actionFormNext(urlNavigate!)
                    
                case .Submit:
                    print("Do Nothing")
                    self.actionFormSubmit(urlNavigate!)
                    
                case .Done:
                    print("Do Done")
                    self.actionFormDone()
                    
                default :
                    println()
                    
                }
            }
            }, onFailure: { (status : Bool, invalidIds: [Dictionary<String, String>]) -> () in
                var strMsg = "Please verify the following fields and try again. \n"
                for errorDict in invalidIds
                {
                    strMsg = strMsg + errorDict.values.array[0] + ".\n"
                }
                
                
                var alert = UIAlertController(title: "Error in input", message: strMsg, preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                println()
        })
    }
    
    
    //MARK:- SWButtonCellDelegte methods
    func buttonClickedFor(cell: SWButtonCell, with fieldData : SWFieldVO)
    {
        if self.delegate != nil
        {
            self.delegate?.formGenerator!(self, buttonTappedWith: cell.button, withFieldId: fieldData.id)
        }
    }
    //MARK:- SWImagePickerCellDelegate methods
    func imagePickDidSelectedFor(cell: SWImagePickerCell, fieldData: SWFieldVO, withCompletion imagePickCompletionBlock:SWImagePickCompletionBlock )
    {
        self.imagePickIndexPath = self.tableView.indexPathForCell(cell)!
        if self.imagePicker == nil
        {
            self.imagePicker = UIImagePickerController()
            self.imagePicker?.delegate = self
        }
        self.imagePicker!.allowsEditing = false
        self.imagePicker!.sourceType = .PhotoLibrary
        self.imagePicCompletionBlock = imagePickCompletionBlock
        
        presentViewController(self.imagePicker!, animated: true, completion: nil)
    }
    
    //MARK:- SWSegmentCellDelegate methods
    func segmentDidChangedFor(cell: SWSegmentCell, selectedIndex: Int,fieldData : SWFieldVO)
    {
        cell.fieldInfo.resultDict = [ cell.fieldInfo.fieldKey : "\(selectedIndex)"]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    //MARK:- SWSliderCellDelegate methods
    func sliderDidChangedFor(cell: SWSliderCell, range: Float,fieldData : SWFieldVO)
    {
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : "\(range)"]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
        
        //        self.dataSource[cell.fieldInfo.fieldKey:"\(range)" ]
        
    }
    
    //MARK:- SWStepperCellDelegate methods
    func steppeDidChangedFor(cell: SWStepperCell, range: Double,fieldData : SWFieldVO)
    {
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : "\(range)"]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    //MARK:- SWSwitchCellDelegate methods
    func switchDidChangedFor(cell: SWSwitchCell, state: UIControlState,fieldData : SWFieldVO)
    {
        var _state = state.rawValue
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : "\(_state)"]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    //MARK:- SWOptionTableViewControllerDelegate methods
    
    func didSelectOption(from: SWOptionTableViewController, options: [Dictionary<String, String>], fieldInfo: SWFieldVO) {
        var indexPath = tableView.indexPathForSelectedRow()
        
        
        if(from.isCheckBoxType) {
           var cell = tableView.cellForRowAtIndexPath(indexPath!) as! SWCheckBoxCell
            cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : options]
            self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
            self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
            cell.seleckedOptions.text = "Some items selected" //String(format: "%@ items selected", options.count)

        } else {
            var cell = tableView.cellForRowAtIndexPath(indexPath!) as! SWRadioButtonCell
            cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : options]
            cell.slectedOption.text = options[0]["title"]

            self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
            self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
        }
    }
    
    
    //MARK:- SWTextFieldCellDataChangeDelegate delegates
    func textFieldCell(cell: SWTextFieldCell, dataChangedWith data: String)
    {
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : data ]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    
    
    
    //MARK:- SWTextViewCellDataChangeDelegate delegates
    func textViewCell(cell: SWTextViewCell, dataChangedWith data: String)
    {
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey :  data]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    // MARK:- UIImagePickerControllerDelegate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let cell = self.tableView.cellForRowAtIndexPath(self.imagePickIndexPath!) as? SWImagePickerCell
            cell!.fieldInfo.resultDict = [cell!.fieldInfo.fieldKey : pickedImage]
            self.dataSource.composedResultDict.removeValueForKey(cell!.fieldInfo.id)
            self.dataSource.composedResultDict[cell!.fieldInfo.id] = cell!.fieldInfo.resultDict!
            self.imagePicCompletionBlock!(pickedImage)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //--------------------------------------
    // MARK: - Convenience
    //--------------------------------------

    func setAlternatingColors(hexColors: String?...){
        self.arrAlternateColors.removeAll(keepCapacity: false)
        if hexColors.count>0{
            for color in hexColors{
                self.arrAlternateColors.append(UIColor.colorFromHex(color!))
            }
        }else{
            self.arrAlternateColors.append(UIColor.whiteColor())
        }
    }
}
