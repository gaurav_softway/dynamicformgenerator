//
//  SWBaseFormGenerator+Actions.swift
//  DynamicFormGenerator
//
//  Created by Gaurav Keshre on 7/2/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
import UIKit

enum SWActionState{
    case BeforeAction
    case OnAction
    case AfterAction
    case AskBeforeAction
}
extension SWBaseFormGenerator{
    
    var isModal: Bool {
        return self.presentingViewController?.presentedViewController == self
            || (self.navigationController != nil && self.navigationController?.presentingViewController?.presentedViewController == self.navigationController)
            || self.tabBarController?.presentingViewController is UITabBarController
    }
    
    //--------------------------------------
    // MARK: - Form NavigationAction
    //--------------------------------------
    /*
    if (self.delegate != nil)
    {
    var shouldSubmit = self.delegate?.formGenerator(self, shouldSubmitJSON: finalJSON, withURL: URL!)
    if (shouldSubmit == false)
    {
    shouldnavigateNextOrPrev = false
    }
    else
    {
    self.delegate?.formGenerator(self, willSubmitJSON: finalJSON, withURL: URL!)
    //                                NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
    //                                    var _formMetaData = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &err) as! NSDictionary
    //                                    dispatch_async(dispatch_get_main_queue(),{
    //                                if (self.delegate != nil)
    //                                {
    //                                    self.delegate?.formGenerator(self, didSubmitJSONwith: _formMetaData, withURL: urlString)
    //                                    check = false
    //                                }
    //                                    })
    //                                });
    
    }
    }
    
    */
    func actionFormDone(){
        self.informDelegateOfFormAction(SWFormAction.Done, on: SWActionState.AfterAction, withObject: "")
    }

    func actionFormSubmit(url: String){
        self.formManager!.addForm(self.dataSource.formMetaData!.id, withFormData: self.getJSONData())
        if (self.informDelegateOfFormAction(SWFormAction.Submit, on: SWActionState.AskBeforeAction, withObject: url) == true)
        {
            self.informDelegateOfFormAction(SWFormAction.Submit, on: SWActionState.BeforeAction, withObject: "")
            self.informDelegateOfFormAction(SWFormAction.Submit, on: SWActionState.OnAction, withObject: "")
            self.informDelegateOfFormAction(SWFormAction.Submit, on: SWActionState.AfterAction, withObject: "")
        }

    }
    
    func actionFormCancel(){
        if self.navigationController != nil
        {
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
        else  if isModal
        {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        else
        {
            self.informDelegateOfFormAction(SWFormAction.Cancel, on: SWActionState.AfterAction, withObject: "")
        }
    }
    
    func actionFormNext(url: String){

        if let app = UIApplication.sharedApplication().delegate as? AppDelegate, let window = app.window {
            SWProgressView.sharedHUD.showInWindow(window, withHeader: nil, andFooter: "Please wait")
        }
        let formStoryBoard: UIStoryboard = UIStoryboard(name: "FormGenerator", bundle: nil)
        let formTVC: SWFormViewController = SWFormViewController.initWith(url,formID: self.dataSource[navigation: "next_form_id"]!, stryboardName: "FormGenerator", viewControllerIdentifier: "SWFormViewController") as! SWFormViewController
        formTVC.delegate = self.delegate
        self.formManager!.addForm(self.dataSource.formMetaData!.id, withFormData: self.getJSONData())
        formTVC.formManager = self.formManager
        self.navigationController?.pushViewController(formTVC, animated: true)
        SWProgressView.sharedHUD.hide()

    }

    
    //--------------------------------------
    // MARK: - Delegation
    //--------------------------------------
    
    func informDelegateOfFormAction(action: SWFormAction, on state:SWActionState, withObject object: AnyObject?)-> Bool?{
        
        if self.delegate == nil {
            return false
        }

        if let delegate = self.delegate{
            switch(action)
            {
            case  SWFormAction.Cancel :
                switch(state)
                {
                case SWActionState.AfterAction:
                    self.delegate?.formGenerator(self, didCancelJSONwith: self.getJSONData())
                    println()
                default :
                    println()
                }
                println()
            case  SWFormAction.Done :
                switch(state)
                {
                case SWActionState.AfterAction:
                    self.delegate?.formGenerator!(self, isDoneWithResultJSON: self.getJSONData())
                    if self.navigationController != nil
                    {
                        self.navigationController?.popToRootViewControllerAnimated(true)
                    }
                    else  if isModal
                    {
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }

                    println()
                default :
                    println()
                }
                println()
            case  SWFormAction.Submit :
                switch(state)
                {
                case SWActionState.AskBeforeAction:
                    return self.delegate?.formGenerator(self, shouldSubmitJSON: self.getJSONData(), withURL: object as! String)
                case SWActionState.BeforeAction:
                    let formCont = self.dataSource.formMetaData!.navigationInfo!["form_count"] as! Int

                        if formCont > 1
                        {
                             self.delegate?.formGenerator(self, willSubmitJSON: self.formManager!.arrFormData, withURL: object as! String)
                        }
                        else
                        {
                             self.delegate?.formGenerator(self, willSubmitJSON: self.getJSONData(), withURL: object as! String)
                        }
                   
                    println()
                case SWActionState.OnAction:
                    println()
                case SWActionState.AfterAction:
                    
                    
                    let formCont = self.dataSource.formMetaData!.navigationInfo!["form_count"] as! Int
                    
                    if formCont > 1
                    {
                        self.delegate?.formGenerator(self, didSubmitJSONwith: self.formManager!.arrFormData, withURL: object as! String)
                    }
                    else
                    {
                       self.delegate?.formGenerator(self, didSubmitJSONwith: self.getJSONData(), withURL: object as! String)
                    }

                    if self.navigationController != nil
                    {
                        self.navigationController?.popToRootViewControllerAnimated(true)
                    }
                    else  if isModal
                    {
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }

                    
                    
                    println()
                default :
                    println()
                }
                println()
            default:
                println()
            }
        }
        return false
    }
   func getJSONData() -> AnyObject
   {
    var dataDict : Dictionary = Dictionary<String, AnyObject>()
        for data in self.dataSource.composedResultDict.values
        {
           
           dataDict[ (data as! Dictionary<String, AnyObject>).keys.array[0] as String] = (data as! Dictionary<String, AnyObject>).values.array[0]
        }
    return dataDict
    }
}
