//
//  SWSegmentCell./swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

protocol SWSegmentCellDelegate{
    func segmentDidChangedFor(cell: SWSegmentCell, selectedIndex: Int,fieldData : SWFieldVO)
}

class SWSegmentCell: SWBaseFieldCell {
    
    var delegate: SWSegmentCellDelegate?
    @IBOutlet weak var segmentController: UISegmentedControl!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func handleSegmentChanged(sender: AnyObject) {
         if self.delegate != nil
         {
            self.delegate?.segmentDidChangedFor(self, selectedIndex: self.segmentController.selectedSegmentIndex, fieldData: self.fieldInfo)
        }
    }
    
    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
        override func configureCellWithMetaData(field: SWFieldVO){
        super.configureCellWithMetaData(field)
        if let arrTitlesDict = field[attribute: "titles"] as? NSArray
        {
            var count = 0
            self.segmentController.removeAllSegments()
            for dataDict in arrTitlesDict{
                if let title = dataDict["title"] as? String{
                    self.segmentController.insertSegmentWithTitle(title, atIndex: count++, animated: false)
                }
                else if let imagePath = dataDict["image"] as? String{
                    self.segmentController.insertSegmentWithImage(UIImage(contentsOfFile: imagePath)!, atIndex: count++, animated: false)
                }
                else{
                    self.segmentController.insertSegmentWithTitle("\(count)", atIndex: count++, animated: false)
                }
            }
            
            if let value: String =  field [attribute: "defaultSelectedItemIndex"] as? String{
                self.segmentController.selectedSegmentIndex =  value.toInt()!
            }
            
        }

        if let colorStr = field [attribute: "stepper_BgColor"] as? String{
            self.segmentController.backgroundColor = UIColor.colorFromHex(colorStr)
        }
        if let colorStr = field [attribute: "segment_tintColor"]  as? String{
            self.segmentController.tintColor = UIColor.colorFromHex(colorStr)
        }
        if let colorStr = field [attribute: "segment_BgColor"] as? String{
            self.segmentController.backgroundColor = UIColor.colorFromHex(colorStr)
        }
    }

}
