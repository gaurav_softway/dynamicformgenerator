//
//  SWImagePickerCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit


protocol SWImagePickerCellDelegate{
    func imagePickDidSelectedFor(cell: SWImagePickerCell, fieldData: SWFieldVO, withCompletion imagePickCompletionBlock:SWImagePickCompletionBlock )
}
typealias SWImagePickCompletionBlock = (UIImage) -> ()

class SWImagePickerCell: SWBaseFieldCell {
    @IBOutlet weak var btnPickImage: UIButton!
    
    var delegate : SWImagePickerCellDelegate?
    @IBOutlet weak var cellImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func updateWithImage(img: UIImage)
    {
        self.cellImageView?.image = img
    }
    
    //--------------------------------------
    // MARK: - Action
    //--------------------------------------

    @IBAction func handlePickImage(sender: AnyObject) {
        if self.delegate != nil
        {
            self.delegate?.imagePickDidSelectedFor(self, fieldData: self.fieldInfo, withCompletion: { (selectedImage:UIImage) -> () in
                self.cellImageView?.image = selectedImage
            })
        }
    }
    
    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
        override func configureCellWithMetaData(field: SWFieldVO){
        super.configureCellWithMetaData(field)


        if let placeHolderString = field.placeholder{
            self.btnPickImage.setTitle(placeHolderString, forState: UIControlState.Normal)
        }
        
        if let value: String = field [attribute: "btn_text_color"] as? String{
            self.btnPickImage.titleLabel?.textColor = UIColor.colorFromHex(value)
        }
        
        if let value: String = field [attribute: "btn_text_BgColor"] as? String{
            self.btnPickImage.backgroundColor = UIColor.colorFromHex(value)
        }
    }
}
