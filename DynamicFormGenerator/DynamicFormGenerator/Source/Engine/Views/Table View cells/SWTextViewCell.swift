//
//  SWTextViewCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

protocol SWTextViewCellDataChangeDelegate{
    func textViewCell(cell: SWTextViewCell, dataChangedWith data: String)
}

class SWTextViewCell: SWBaseFieldCell, UITextViewDelegate {
    
    var delegate : SWTextViewCellDataChangeDelegate?
     
    @IBOutlet weak var txtView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //--------------------------------------
    // MARK: - UITextViewDelegate
    //--------------------------------------

 
    func textViewDidEndEditing(textView: UITextView)
    {
        
    }
    
    func textViewDidChange(textView: UITextView)
    {
        if (self.delegate != nil)
        {
            self.delegate!.textViewCell(self, dataChangedWith: self.txtView.text)
        }
    }
    func textFieldDidEndEditing(textField: UITextField)
    {
        if (self.delegate != nil)
        {
            self.delegate!.textViewCell(self, dataChangedWith: self.txtView.text)
        }
    }
    
    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
        override func configureCellWithMetaData(field: SWFieldVO){
        super.configureCellWithMetaData(field)
        
            self.txtView.text = field.placeholder ?? ""
            self.txtView.editable = field.enabled

        if let textAlignment: String = field[attribute: "text_alignment"] as? String {
            self.txtView.textAlignment = NSTextAlignment(rawValue:(textAlignment.toInt())!)!
        }
        
        if let textClass: String = field[attribute: "text_class"] as? String{
            self.txtView.font = UIFont(styleTag: textClass, fontFamily: nil)
        }
        if let color = field[attribute: "text_color"] as? String
        {
            self.txtView.textColor = UIColor.colorFromHex(color)
        }
        
        if let color: String  = field[attribute: "text_BgColor"] as? String{
            self.txtView.backgroundColor = UIColor.colorFromHex(color)
        }
    }

}
