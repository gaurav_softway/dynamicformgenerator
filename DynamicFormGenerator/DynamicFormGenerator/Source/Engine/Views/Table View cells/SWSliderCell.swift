//
//  SWSliderCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit
protocol SWSliderCellDelegate{
    func sliderDidChangedFor(cell: SWSliderCell, range: Float,fieldData : SWFieldVO)
}

class SWSliderCell: SWBaseFieldCell {
    
    var delegate: SWSliderCellDelegate?

    @IBOutlet weak var lblCurrentValue: UILabel!
     @IBOutlet weak var lblMin: UILabel!
     @IBOutlet weak var lblMax: UILabel!
    @IBOutlet weak var slider: UISlider!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func handleSliderdidChanged(sender: AnyObject) {
        let lblText = self.fieldInfo.caption + ": \(self.slider.value)"
        self.lblCurrentValue.text  = lblText
        if (self.delegate as? String != nil)
        {
            self.delegate?.sliderDidChangedFor(self, range: self.slider.value, fieldData: self.fieldInfo)
        }
    }
    
    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
        override func configureCellWithMetaData(field: SWFieldVO){
        super.configureCellWithMetaData(field)


        self.slider.minimumValue = Float(field[ attribute: "min"] as! NSNumber) ?? 0
        self.slider.maximumValue = Float(field[ attribute: "max"] as! NSNumber) ?? 10.0
        self.slider.value = Float(field[ attribute: "current"] as! NSNumber) ?? 0.0
        let lblText = field.caption + ": \(self.slider.value)"
            
        self.lblCurrentValue.text = lblText

        self.lblMin.text = field[ attribute: "min_label"] as? String ?? "Min"
        
        self.lblMax.text  = field[ attribute: "max_label"] as? String ?? "Max"
        
        
        if let value = field[ attribute: "min_track_tint_color"] as? String{
            self.slider.minimumTrackTintColor = UIColor.colorFromHex(value)
        }
        if let value = field[ attribute: "max_track_tint_color"] as? String{
            self.slider.maximumTrackTintColor = UIColor.colorFromHex(value)
        }
        if let value = field[ attribute: "slider_tintColor"] as? String{
            self.slider.tintColor = UIColor.colorFromHex(value)
        }
        if let value = field[ attribute: "slider_BgColor"] as? String{
            self.slider.backgroundColor = UIColor.colorFromHex(value)
        }
        if let value = field[ attribute: "min_image"] as? String{
            self.slider.minimumValueImage = UIImage(contentsOfFile: field[ attribute: "min_image"] as! String)
        }
        if let value = field[ attribute: "max_image"] as? String{
            self.slider.maximumValueImage = UIImage(contentsOfFile:field[ attribute: "max_image"] as! String)
        }

    }

}
