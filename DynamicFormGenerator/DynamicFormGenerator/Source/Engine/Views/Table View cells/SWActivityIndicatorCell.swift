//
//  SWActivityIndicatorCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

class SWActivityIndicatorCell: SWBaseFieldCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Animation Methods
    func startAnimating()
    {
        self.activityIndicator.startAnimating()
    }
    func stopAnimatiog()
    {
        self.activityIndicator.stopAnimating()
    }
    
    
    //--------------------------------------
    // MARK: - Configure
    //--------------------------------------
    
        override func configureCellWithMetaData(field: SWFieldVO){
        super.configureCellWithMetaData(field)        
            if let value = field[attribute: "activity_startAnimating"] as? NSNumber where value.integerValue == 0 {
                self.startAnimating()
            }
    }
}
