//
//  SWRadioButtonTableViewController.swift
//  DynamicFormGenerator
//
//  Created by Sugeet on 29/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit


protocol SWOptionTableViewControllerDelegate{
    func didSelectedOption(selectedOption: Dictionary<String, String>, with fieldInfo : SWFieldVO)
    func didSelectedOptionsWith(selectedOptions:[Dictionary<String, String>], with fieldInfo: SWFieldVO)
    
    func didSelectOption(from: SWOptionTableViewController, options: [Dictionary<String, String>], fieldInfo: SWFieldVO)
}

class SWOptionTableViewController: UITableViewController {
    var fieldInfo: SWFieldVO!
    var navTitle: String?
    var isCheckBoxType = false
    var delegate : SWOptionTableViewControllerDelegate?


    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.navTitle
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fieldInfo.options.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCellWithIdentifier("OptionsCell", forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.text = self.fieldInfo.options[indexPath.row]["title"]
        
        println(self.fieldInfo.resultDict)
        
        if isCheckBoxType {
            
        var tempArray = self.fieldInfo.resultDict!["selectedOptions"] as! [Dictionary<String, String>]
            
//        if(contains(tempArray, self.fieldInfo.options[indexPath.row]["id"] as String)) {
//            
//        }
//        } else {
//            if(contains(self.fieldInfo.options[indexPath.row]["id"], self.fieldInfo.resultDict!["select_option"])) {
//                
//            }
//        }
        }
        
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if isCheckBoxType == true {
            tableView.allowsMultipleSelection = true
            if(tableView.cellForRowAtIndexPath(indexPath)?.accessoryType == UITableViewCellAccessoryType.Checkmark) {
                tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = UITableViewCellAccessoryType.None
            } else {
                tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
            
        } else {
            tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
            tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = UITableViewCellAccessoryType.None
    }
    
    @IBAction func handleDone(sender: AnyObject) {

            var selectedOptions = [Dictionary<String, String>]()
            
            if let selectedIndexPaths = tableView.indexPathsForSelectedRows() as? [NSIndexPath] {
                
                if isCheckBoxType == true {

                for indexPath in selectedIndexPaths {
                    
                    if tableView.cellForRowAtIndexPath(indexPath)?.accessoryType == UITableViewCellAccessoryType.Checkmark {
                        selectedOptions.append(self.fieldInfo.options[indexPath.row] as Dictionary<String, String>)
                    }
                }
                if self.delegate != nil {
                    self.delegate?.didSelectOption(self, options: selectedOptions, fieldInfo: self.fieldInfo)
                }
            } else  {
                var indexPath = selectedIndexPaths[0] as NSIndexPath
                if tableView.cellForRowAtIndexPath(indexPath)?.accessoryType == UITableViewCellAccessoryType.Checkmark {
                    selectedOptions.append(self.fieldInfo.options[indexPath.row] as Dictionary<String, String>)
                }
                if self.delegate != nil {
                    self.delegate?.didSelectOption(self, options: selectedOptions, fieldInfo: self.fieldInfo)
                }
            }
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
}
