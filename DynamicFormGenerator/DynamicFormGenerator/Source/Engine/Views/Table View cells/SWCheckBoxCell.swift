//
//  SWCheckBoxCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit


class SWCheckBoxCell: SWBaseFieldCell, UICollectionViewDelegate
{
    @IBOutlet weak var seleckedOptions: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
    override func configureCellWithMetaData(fieldData: SWFieldVO){
        super.configureCellWithMetaData(fieldData)

        //To be overridden by sublcasses
        super.configureCellWithMetaData(fieldData)
    }

    
}
