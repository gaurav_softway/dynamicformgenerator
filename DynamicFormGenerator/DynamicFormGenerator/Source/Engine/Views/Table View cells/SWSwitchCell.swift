//
//  SWSwitchCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit
protocol SWSwitchCellDelegate{
    func switchDidChangedFor(cell: SWSwitchCell, state: UIControlState,fieldData : SWFieldVO)
}
class SWSwitchCell: SWBaseFieldCell {

    var delegate: SWSwitchCellDelegate?

    @IBOutlet weak var btnSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func handleSwitchStateChanged(sender: AnyObject) {
        if (self.delegate as? String != nil)
        {
            self.delegate?.switchDidChangedFor(self, state: self.btnSwitch.state, fieldData: self.fieldInfo)
        }
    }
    
    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
        override func configureCellWithMetaData(field: SWFieldVO){
        super.configureCellWithMetaData(field)


        if let value: String = field[ attribute: "defaultState"] as? String{
          self.btnSwitch.setOn((value.toInt() == 0), animated: true)
        }
        if let value: String = field[ attribute: "on_tintColor"] as? String{
            self.btnSwitch.onTintColor = UIColor.colorFromHex(value)
        }
        if let value: String = field[ attribute: "thum_tintColor"] as? String{
            self.btnSwitch.thumbTintColor = UIColor.colorFromHex(value)
        }
        
        if let value = field[ attribute: "off_image"] as? String{
            self.btnSwitch.offImage = UIImage(contentsOfFile: value)
        }
        if let value = field[ attribute: "on_image"] as? String{
            self.btnSwitch.offImage = UIImage(contentsOfFile: value)
        }
        if let value = field[ attribute: "switch_tintColor"] as? String{
            self.btnSwitch.tintColor = UIColor.colorFromHex(value)
        }
        if let value = field[ attribute: "switch_BgColor"] as? String{
            self.btnSwitch.backgroundColor = UIColor.colorFromHex(value)
        }

    }

}
