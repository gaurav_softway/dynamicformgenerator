//
//  ViewController.swift
//  DynamicFormGenerator
//
//  Created by Gaurav Keshre on 6/16/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit
import DynamicFormGeneratorLib

let URL = "http://www.json-generator.com/api/json/get/cvssGscHIi"
let STORYBOARDNAME = "FormGenerator"
let VIEWCONTROLLERID = "SWBaseFormGenerator"

class ViewController: UIViewController, SWFormGeneratorDelegate {
    
    var data = NSMutableData()
    var formMetaData: Dictionary<String,AnyObject>!
    var recievedData : NSMutableData?
    var formManager : SWFormManager?
    override func viewDidLoad() {

        var err: NSError?
        
        let url = NSURL(string:"http://www.json-generator.com/api/json/get/bXwxwPzQaG?indent=2")//"http://www.json-generator.com/api/json/get/bQdAfBOfYi?indent=2")//"http://www.json-generator.com/api/json/get/cgCzSoNjEy?indent=2")//"http://www.json-generator.com/api/json/get/cpKYgILmyG?indent=2")//"http://www.json-generator.com/api/json/get/cdXCGkMhwy?indent=2")//"http://www.json-generator.com/api/json/get/bTzsRcTJua?indent=2")//"http://www.json-generator.com/api/json/get/bUrZNQUUmW?indent=2")//"http://www.json-generator.com/api/json/get/bOIaLbQXSa?indent=2")//"http://www.json-generator.com/api/json/get/coGMZKAioO?indent=2")//"http://www.json-generator.com/api/json/get/cpCnmrkebm?indent=2")//"http://www.json-generator.com/api/json/get/cmkwqubjLS?indent=2")//"http://www.json-generator.com/api/json/get/cluOVUCAbS?indent=2")//"http://www.json-generator.com/api/json/get/coIPGSPXJu?indent=2")//"http://www.json-generator.com/api/json/get/cguwNXZRfm?indent=2")// "http://www.json-generator.com/api/json/get/caPjVnCVrC?indent=2")
        let request = NSURLRequest(URL: url!)
        let queue : NSOperationQueue = NSOperationQueue()


        
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
          self.formMetaData = (try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)) as! Dictionary<String, AnyObject>
        })
}
    
//MARK:- action methods
     func handleShowForm(sender: AnyObject) {
        self.formManager = SWFormManager()
        let formTVC: SWBaseFormGenerator = SWBaseFormGenerator.initWith(URL, stryboardName: STORYBOARDNAME, viewControllerIdentifier: VIEWCONTROLLERID) as! SWBaseFormGenerator
        formTVC.delegate = self
        formTVC.formManager = self.formManager
        self.navigationController?.pushViewController(formTVC, animated: true)
        
    }
    
    //MARK:- SWBaseFormGeneratorDelegate methodes
    func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingJSONWith jsonData: Dictionary<String,AnyObject>) {
         if let app = UIApplication.sharedApplication().delegate as? AppDelegate, let window = app.window {
            SWProgressView.sharedHUD.showInWindow(window, withHeader: nil, andFooter: "Please wait")
        }
        print("\(__FUNCTION__)")
    }
        
    func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingURLWith URL: String) {
         if let app = UIApplication.sharedApplication().delegate as? AppDelegate, let window = app.window {
            SWProgressView.sharedHUD.showInWindow(window, withHeader: nil, andFooter: "Please wait")
        }
        print("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, doneJSONLoadWith jsonData: Dictionary<String,AnyObject>?) {
        SWProgressView.sharedHUD.hide()
        print("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, startedFormRenderingWith jsonData: Dictionary<String,AnyObject>) {
        print("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, doneFormRenderingWith jsonData: Dictionary<String,AnyObject>) {
        SWProgressView.sharedHUD.hide()
        print("\(__FUNCTION__)")
    }
    
    func formGenerator( formGenerator:SWBaseFormGenerator, isPreparingForNext : Bool, withStartedLoadingURL URL: String) {
        print("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, isPreparingForPrevious : Bool, withStartedLoadingURL URL: String) {
        print("\(__FUNCTION__)")
    }
    
    func formGenerator( formGenerator:SWBaseFormGenerator, isDonePressed : Bool, withResultJSON json: Dictionary<String,AnyObject>) {
        print("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, buttonTappedWith button : UIButton, withFieldId id: String) {
         print("\(__FUNCTION__)")
    }
    
    //MARK:- SWformGeneratorCancelDelegate methods
    func formGenerator( formGenerator:SWBaseFormGenerator, shouldCancelJSON jsonData: Dictionary<String,AnyObject>, withURL:String) -> Bool {
        if let app = UIApplication.sharedApplication().delegate as? AppDelegate, let window = app.window {
            SWProgressView.sharedHUD.showInWindow(window, withHeader: nil, andFooter: "Please wait")
        }
        return true
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, willCancelJSON jsonData: Dictionary<String,AnyObject>, withURL:String)
    {
        print("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, didCancelJSONwith resultJSON: Dictionary<String,AnyObject>, withURL:String)
    {
        SWProgressView.sharedHUD.hide()
        print("\(__FUNCTION__)")
        formGenerator.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    //MARK:- SWBaseFormGeneratorCancelDelegate Methodes
    func formGenerator( formGenerator:SWBaseFormGenerator, shouldSubmitJSON jsonData: Dictionary<String,AnyObject>, withURL:String) -> Bool {
        if let app = UIApplication.sharedApplication().delegate as? AppDelegate, let window = app.window {
            SWProgressView.sharedHUD.showInWindow(window, withHeader: nil, andFooter: "Please wait")
        }
        return true
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, willSubmitJSON jsonData: Dictionary<String,AnyObject>, withURL:String) {
        print("\(__FUNCTION__)")
    }
    func formGenerator( formGenerator:SWBaseFormGenerator, didSubmitJSONwith resultJSON: Dictionary<String,AnyObject>, withURL:String) {
        SWProgressView.sharedHUD.hide()
        print("\(__FUNCTION__)")
        formGenerator.navigationController?.popToRootViewControllerAnimated(true)
    }
 }
