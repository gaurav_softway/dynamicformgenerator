//
//  SWButtonCell.swif
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

protocol SWButtonCellDelegte{
    func buttonClickedFor(cell: SWButtonCell, with fieldData : SWFieldVO)
}

class SWButtonCell: SWBaseFieldCell {
    
    var delegate: SWButtonCellDelegte?
    
    @IBOutlet weak var button: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //--------------------------------------
    // MARK: - ACTION
    //--------------------------------------

    @IBAction func handleBtnAction(sender: AnyObject) {
        if let de = self.delegate {
            de.buttonClickedFor(self, with: self.fieldInfo)
        }
    }
    
    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
    override func configureCellWithMetaData(fieldData: SWFieldVO) {
        self.fieldInfo = fieldData
        
        if let strPlaceHolder = fieldData.placeholder {
            self.button.setTitle(strPlaceHolder, forState: UIControlState.Normal)
        }
        if let strBtnTextColor: String = fieldData[attribute:"btn_text_color"]  as? String{
            self.button.titleLabel?.textColor = UIColor.colorFromHex(strBtnTextColor)
        }
        if let strBGColor: String = fieldData[attribute:"btn_text_BgColor"] as? String{
            self.button.backgroundColor = UIColor.colorFromHex(strBGColor)
        }
        
    }

}
