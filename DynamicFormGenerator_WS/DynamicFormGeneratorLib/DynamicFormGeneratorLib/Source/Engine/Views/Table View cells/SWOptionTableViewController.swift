//
//  SWRadioButtonTableViewController.swift
//  DynamicFormGenerator
//
//  Created by Sugeet on 29/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit


protocol SWOptionTableViewControllerDelegate{
    func didSelectedOption(selectedOption: Dictionary<String, String>, with fieldInfo : SWFieldVO)
    func didSelectedOptionsWith(selectedOptions:[Dictionary<String, String>], with fieldInfo: SWFieldVO)
    
//    func didSelectOption(from: SWOptionTableViewController, options: [Dictionary<String, String>], fieldInfo: SWFieldVO)
}

class SWOptionTableViewController: UITableViewController {
    var fieldInfo: SWFieldVO!
    var navTitle: String?
    var isCheckBoxType = false
    var selectedOptions = [Dictionary<String, String>]()
    var select_option = Dictionary<String, String>()
    var delegate : SWOptionTableViewControllerDelegate?


    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.navTitle
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fieldInfo.options.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCellWithIdentifier("OptionsCell", forIndexPath: indexPath) 
        cell.textLabel?.text = self.fieldInfo.options[indexPath.row]["title"]
        
        if isCheckBoxType {
            for option in self.selectedOptions {
                if(self.fieldInfo.options[indexPath.row]["id"] == option["id"]) {
                    cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                }
            }
        } else  {
            if(self.fieldInfo.options[indexPath.row]["id"] == select_option["id"]) {
                cell.accessoryType = UITableViewCellAccessoryType.Checkmark
                tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.Top)
            }
        }
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if isCheckBoxType == true {
            tableView.allowsMultipleSelection = true
            if(tableView.cellForRowAtIndexPath(indexPath)?.accessoryType == UITableViewCellAccessoryType.Checkmark) {
                var index = 0
                for option in self.selectedOptions {
                    if(option["id"] == self.fieldInfo.options[indexPath.row]["id"]) {
                        self.selectedOptions.removeAtIndex(index)
                        tableView.cellForRowAtIndexPath(indexPath)?.selected = false
                        tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = UITableViewCellAccessoryType.None
                        break
                    }
                    index++
                }
            }
            else {
                tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = UITableViewCellAccessoryType.Checkmark
            }
        }
        else {
//            var indexPath1 = tableView.indexPathForSelectedRow
            tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = UITableViewCellAccessoryType.Checkmark
        }
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
            tableView.cellForRowAtIndexPath(indexPath)?.accessoryType = UITableViewCellAccessoryType.None
    }
    
    @IBAction func handleDone(sender: AnyObject) {
        
        if isCheckBoxType == true {
            if let selectedIndexPaths = tableView.indexPathsForSelectedRows as? [NSIndexPath] {
                for indexPath in selectedIndexPaths {
                    if tableView.cellForRowAtIndexPath(indexPath)?.accessoryType == UITableViewCellAccessoryType.Checkmark {
                        selectedOptions.append(self.fieldInfo.options[indexPath.row] as Dictionary<String, String>)
                    }
                }
                if self.delegate != nil {
                    self.delegate?.didSelectedOptionsWith(selectedOptions, with: self.fieldInfo)
                }
            }
        } else {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                var selectedOption = self.fieldInfo.options[selectedIndexPath.row] as Dictionary<String, String>
                if self.delegate != nil {
                    self.delegate?.didSelectedOption(selectedOption, with: self.fieldInfo)
                }
            }
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
}
