//
//  SWStepperCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit
protocol SWStepperCellDelegate{
    func steppeDidChangedFor(cell: SWStepperCell, range: Double,fieldData : SWFieldVO)
}
class SWStepperCell: SWBaseFieldCell {

    @IBOutlet weak var lblCurrentValue: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    var delegate: SWStepperCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func handleStepperDidChanged(sender: AnyObject) {
        self.lblCurrentValue.text = "\(self.stepper.value)"
        if (self.delegate as? String != nil)
        {
            self.delegate?.steppeDidChangedFor(self, range: self.stepper.value, fieldData: self.fieldInfo)
        }
    }
    
    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
        override func configureCellWithMetaData(field: SWFieldVO){
        super.configureCellWithMetaData(field)

        
        self.stepper.minimumValue = Double(field[attribute: "min"] as! NSNumber) ?? 0.0
        self.stepper.maximumValue = Double(field[attribute: "max"] as! NSNumber) ?? 10.0
        
        self.lblCurrentValue.text = "0.0"

        if let value = field[attribute: "stepper_tintColor"] as? String{
            self.stepper.tintColor = UIColor.colorFromHex(value)
        }
        if let value = field[attribute: "stepper_BgColor"] as? String{
            self.stepper.backgroundColor = UIColor.colorFromHex(value)
        }
        
    }

}
