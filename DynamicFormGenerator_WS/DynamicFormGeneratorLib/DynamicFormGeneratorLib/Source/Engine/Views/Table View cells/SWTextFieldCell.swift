//
//  SWTextFieldCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

protocol SWTextFieldCellDataChangeDelegate{
    func textFieldCell(cell: SWTextFieldCell, dataChangedWith data: String)
}

class SWTextFieldCell: SWBaseFieldCell , UITextFieldDelegate{
    var dataChangeDelegate : SWTextFieldCellDataChangeDelegate?
//   override var fieldInfo : SWFieldVO!
//        {
//        get{
//            return self.fieldInfo
//            }
//        set(newVal)
//        {
//            self.fieldInfo = newVal
//            if newVal.needsValidation
//            {
//                self.txtField.delegate = self
//            }
//        }
//    }
    
    @IBOutlet weak var txtField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    //MARK:- UITextFieldDelegate methods
    func textFieldDidEndEditing(textField: UITextField)
    {
        if (self.dataChangeDelegate != nil)
        {
            self.dataChangeDelegate!.textFieldCell(self, dataChangedWith: self.txtField.text!)
        }
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
//        var textFieldString: String!
//        if (range.length == 0)
//        {
//            textFieldString = self.txtField.text! + string
//        }
//        else
//        {
//           textFieldString = self.txtField.text!.substringWithRange(Range<String.Index>(start: self.txtField.text!.startIndex, end: self.txtField.text!.endIndex.advancedBy(-range.length))) //"Hello, playground"
//        }
        if (self.dataChangeDelegate != nil)
        {
            self.dataChangeDelegate!.textFieldCell(self, dataChangedWith: self.txtField.text!)
        }
        return true
    }
    
    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
        override func configureCellWithMetaData(field: SWFieldVO){
        super.configureCellWithMetaData(field)

        //To be overridden by sublcasses
        if let textAlignment: String = field[attribute: "text_alignment"] as? String {
            self.txtField.textAlignment = NSTextAlignment(rawValue:(Int(textAlignment))!)!
        }
        if let textClass: String = field[attribute: "text_class"]  as? String{
            self.txtField.font = UIFont(styleTag: textClass, fontFamily: nil)
        }
        
        if let textColor: String = field[attribute: "text_color"]as? String{
            self.txtField.textColor = UIColor.colorFromHex(textColor)
        }
        
        if let textBGColor: String = field[attribute: "text_BgColor"] as? String{
            self.txtField.backgroundColor = UIColor.colorFromHex(textBGColor)
        }
    }

}
