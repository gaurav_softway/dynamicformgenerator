//
//  SWBaseFieldCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 16/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

protocol ConfigurableFieldCell {
    //    var delegate: ConfigurableFieldCell { get set }
    func configureCellWithMetaData(field: SWFieldVO)
}

class SWBaseFieldCell: UITableViewCell, ConfigurableFieldCell {
    var fieldInfo : SWFieldVO!
    var canTakeFocus: Bool!
    
    @IBOutlet weak var lblCaption: UILabel!
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    //--------------------------------------
    // MARK: - ConfigurableFieldCell
    //--------------------------------------
    
    func configureCellWithMetaData(field: SWFieldVO){
        self.fieldInfo = field
        if let value = field[attribute: "label_class"] as? String
        {
            var uiFont = UIFont(styleTag: value, fontFamily: nil)
            
            if self.lblCaption == nil{
                (self as! SWSliderCell).lblMin.font = UIFont(styleTag: field[attribute: "label_class"] as! String, fontFamily: nil)
                (self as! SWSliderCell).lblMax.font = UIFont(styleTag: field[attribute: "label_class"] as! String, fontFamily: nil)
            }else{
                self.lblCaption.font = UIFont(styleTag:value, fontFamily: nil)
            }
        }
        
        if let value = field[attribute: "label_color"] as? String{
            if self.lblCaption == nil{
                (self as! SWSliderCell).lblMin.textColor = UIColor.colorFromHex(value)
                (self as! SWSliderCell).lblMax.textColor = UIColor.colorFromHex(value)
            }else{
                self.lblCaption.textColor = UIColor.colorFromHex(value)
            }
        }
        
        if self.lblCaption != nil{
            self.lblCaption.text = field.caption
        }
        if field.required{
            if self.lblCaption != nil{
                var aString : NSMutableAttributedString!
                let index = self.lblCaption.text!.endIndex.advancedBy(-1)
                let character =  self.lblCaption.text![index]
                let attrs = [NSBaselineOffsetAttributeName : 10]
                if character == "*"
                {
                    aString = NSMutableAttributedString(string:self.lblCaption.text!, attributes:attrs)
                }else{
                    aString = NSMutableAttributedString(string:self.lblCaption.text! + "*", attributes:attrs)
                }
                aString.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: NSMakeRange(aString.string.characters.count - 1, 1))
                self.lblCaption.attributedText = aString
            }
        }
    }
    
    //--------------------------------------
    // MARK: - Delegate setter
    //--------------------------------------

    
    func setDelegate(type: SWFormFieldType, delegate: AnyObject?){
        switch type{
        case .Button:
            (self as! SWButtonCell).delegate = delegate as? SWButtonCellDelegte
            
        case .ImagePicker:
            (self as! SWImagePickerCell).delegate = delegate as? SWImagePickerCellDelegate
            
        case .SegmentControl:
            (self as! SWSegmentCell).delegate = delegate as? SWSegmentCellDelegate
            
        case .Switch:
            (self as! SWSwitchCell).delegate = delegate as? SWSwitchCellDelegate
            
        case .Slider:
            (self as! SWSliderCell).delegate = delegate as? SWSliderCellDelegate
            
        case .Stepper:
            (self as! SWStepperCell).delegate = delegate as? SWStepperCellDelegate
        default:
            return;
        }
    }
    
    
}
