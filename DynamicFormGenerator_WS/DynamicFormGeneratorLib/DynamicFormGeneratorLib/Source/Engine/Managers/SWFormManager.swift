//
//  SWFormManager.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 22/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

public class SWFormManager: NSObject {

    lazy var arrFormData = [Dictionary<String, Dictionary<String, String>>]()
    
    func addForm(form_id: String, withFormData form_data: Dictionary<String,
 String>){
    let recordForm = [form_id: form_data] as Dictionary
    self.arrFormData.append(recordForm)
    }
    
    func dataForFormID(form_id: String) -> Dictionary<String, String>{
        
        var returnData: Dictionary<String, String> =  Dictionary<String, String>()
        
        arrFormData.filter { (data:Dictionary) -> Bool in
            if let vd = data[form_id]{
                returnData = vd
                return true;
            }
            return false
        }
        return returnData
    }
}
