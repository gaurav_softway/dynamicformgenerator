//
//  SWDataManager.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 01/07/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit
import Foundation
import CoreData

let LAST_MODIFIED_DATE_ON_SERVICE = "Date"
let DATE_FORMAT = "EEE, dd MM yyyy HH:mm:ss zzz"
typealias SWDataManagerCompletionBlock = (Bool, AnyObject?) -> ()

class SWDataManager: NSObject {
    
    var responseBlock: SWDataManagerCompletionBlock?
    var completionBlock: SWDataManagerCompletionBlock?
    var isDataAlreadyExists = false
    var formId: String?
    let queue : NSOperationQueue = NSOperationQueue()
    var responseData = NSMutableData()
    
    
    class var sharedInstance : SWDataManager {
        struct Static {
            static var instance : SWDataManager?
            static var token : dispatch_once_t = 0
        }
        dispatch_once(&Static.token) {
            Static.instance = SWDataManager()
        }
        return Static.instance!
    }
    
    func getJSONForFormId(id: String?, withURL url: String?, onCompletion completionBlock: SWDataManagerCompletionBlock?){
        self.completionBlock = completionBlock
        self.formId = id
        if isDataExistsInLocalFor(FormId: id!)
        {
            isDataAlreadyExists = true
            self.createConnection(URL: url!, withResponseCallBack: { (status:Bool, response:AnyObject?) -> () in
                self.completionBlock!(status,response)
            })
//            let data = self.getJSONDataInLocalFor(URL:url!)
//            self.completionBlock!(true, data)
        }
        else
        {
            self.createConnection(URL: url!, withResponseCallBack: { (status:Bool, response:AnyObject?) -> () in
                self.completionBlock!(status,response)
            })
        }
        
    }
    func isDataExistsInLocalFor(FormId id: String) -> Bool
    {
        
        // Check Data exist for formId in the core data, if yes then return true else flase
        
        // Fecth from core data for the given id and if data exist then return true
        
        let formData = SWFormMO.getFormData(id)
        
        if formData?.count>0 {
            return true
        }
        else {
            return false
        }
    }
    
    func getJSONDataInLocalFor(URL urs: String) -> Dictionary<String,AnyObject>
    {
        return Dictionary<String,AnyObject>()
    }
    func createConnection(URL url: String, withResponseCallBack responseCallBack: SWDataManagerCompletionBlock)
    {
        self.responseBlock = responseCallBack
        let URL = NSURL(string:url)
        let request = NSURLRequest(URL: URL!)
        var error: NSError?
        let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: true)!
        connection.start()
        
    }
    
    //MARK: - NSURLConnectionDelegate methods
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        
        // Check the last modified date on service and local last modified date, if local last modified date is smaller then last modified date on servie then download the data else cancel the connection and return the local data on response block
        
        var lastModifiedDateOnService: NSDate = NSDate()
        
        if let httpResponse = response as? NSHTTPURLResponse {
            if let dateString = httpResponse.allHeaderFields[LAST_MODIFIED_DATE_ON_SERVICE] as? String {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = DATE_FORMAT
                lastModifiedDateOnService = dateFormatter.dateFromString(dateString)!
                dateFormatter.timeZone = NSTimeZone.localTimeZone()
                print(dateFormatter.stringFromDate(lastModifiedDateOnService))
            }
        }
        
        if isDataAlreadyExists {
            var formData = SWFormMO.getFormData(self.formId!)
            
        }
        
        
        
//        let currentDate : NSDate = NSDate()
//        let compareResult = date.compare(currentDate)
       
        
//        if compareResult == NSComparisonResult.OrderedAscending{
//            connection.cancel()
//            // get from local
//            self.responseBlock!(false, nil)
//        }
    }
    func connection(connection: NSURLConnection, didSendBodyData bytesWritten: Int, totalBytesWritten: Int, totalBytesExpectedToWrite: Int) {
        var err: NSError?
        var headerData = (try! NSJSONSerialization.JSONObjectWithData(self.responseData, options: NSJSONReadingOptions.AllowFragments)) as! Dictionary<String,AnyObject>
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!){
        self.responseData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!)
    {
        let err: NSError?
        if self.responseData.length > 0 
        {
            var formMetaData = Dictionary<String,AnyObject>()
            
            if let tempFormMetaData = NSJSONSerialization.JSONObjectWithData(self.responseData, options: NSJSONReadingOptions.AllowFragments) as? Dictionary<String,AnyObject> {
                formMetaData = tempFormMetaData
            }
            if err != nil
            {
                self.responseBlock!(false, err)
            }
            else
            {
                self.responseBlock!(true, formMetaData)
                SWFormMO.insertFormDataWith("123", FormPath: "path1", AndLastModifiedDate: NSDate(), inManagedObjectContext: CoreDataManager.mainManagedObjectContext())
                SWFormMO.insertFormDataWith("124", FormPath: "path2", AndLastModifiedDate: NSDate(), inManagedObjectContext: CoreDataManager.mainManagedObjectContext())
                SWFormMO.insertFormDataWith("125", FormPath: "path3", AndLastModifiedDate: NSDate(), inManagedObjectContext: CoreDataManager.mainManagedObjectContext())
                
//                print(SWFormMO.getFormData(123))
//                print(SWFormMO.getFormData(124))
//                print(SWFormMO.getFormData(125))
//
//                SWFormMO.deleteFormDataForId(123)
//                
//                print(SWFormMO.getFormData(123))
//                print(SWFormMO.getFormData(124))
//                print(SWFormMO.getFormData(125))

            }
        }
        else
        {
            self.responseBlock!(false, err)
        }
    }
}
