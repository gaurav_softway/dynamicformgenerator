//
//  SWBaseFormGenerator.swift
//  DynamicFormGenerator
//
//  Created by Gaurav Keshre on 6/16/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
import UIKit

enum SWBaseFormGeneratorSourceType : Int
{
    case NONE
    case JSON
    case URL
    case LOCAL_FILE
}

@objc public protocol SWFormGeneratorCancelDelegate{
    func formGenerator( formGenerator:SWBaseFormGenerator, shouldCancelJSON jsonData: Dictionary<String,AnyObject>, withURL:String) -> Bool
    func formGenerator( formGenerator:SWBaseFormGenerator, willCancelJSON jsonData: Dictionary<String,AnyObject>, withURL:String)
    func formGenerator( formGenerator:SWBaseFormGenerator, didCancelJSONwith resultJSON: Dictionary<String,AnyObject>, withURL:String)
}

@objc public protocol SWFormGeneratorSubmitDelegate{
    func formGenerator( formGenerator:SWBaseFormGenerator, shouldSubmitJSON jsonData: Dictionary<String,AnyObject>, withURL:String) -> Bool
    func formGenerator( formGenerator:SWBaseFormGenerator, willSubmitJSON jsonData: Dictionary<String,AnyObject>, withURL:String)
    func formGenerator( formGenerator:SWBaseFormGenerator, didSubmitJSONwith resultJSON: Dictionary<String,AnyObject>, withURL:String)
}

@objc public protocol SWFormGeneratorDelegate: SWFormGeneratorSubmitDelegate, SWFormGeneratorCancelDelegate{
    
    optional func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingJSONWith jsonData: Dictionary<String,AnyObject>)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, startedLoadingURLWith URL: String)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, doneJSONLoadWith jsonData: Dictionary<String,AnyObject>?)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, startedFormRenderingWith jsonData: Dictionary<String,AnyObject>)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, doneFormRenderingWith jsonData: Dictionary<String,AnyObject>)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, isPreparingForNext : Bool, withStartedLoadingURL URL: String)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, isPreparingForPrevious : Bool, withStartedLoadingURL URL: String)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, isDonePressed : Bool, withResultJSON JSON: Dictionary<String,AnyObject>)
    optional func formGenerator( formGenerator:SWBaseFormGenerator, buttonTappedWith button : UIButton, withFieldId id: String)
}



public class SWBaseFormGenerator: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SWButtonCellDelegte, SWImagePickerCellDelegate,SWSegmentCellDelegate,SWSliderCellDelegate,SWStepperCellDelegate,SWSwitchCellDelegate, SWTextFieldCellDataChangeDelegate , SWTextViewCellDataChangeDelegate, SWOptionTableViewControllerDelegate{
    
    
    weak public var delegate: SWFormGeneratorDelegate?
    var showAlternateColors: Bool? = false
    var formMetaData: Dictionary<String,AnyObject>!
    var formMetaDataURL: String!
    
    var imagePicker :UIImagePickerController?
    var imagePicCompletionBlock:SWImagePickCompletionBlock?
    var validationCompleteBlock:SWValidationCompletionBlock?
    
    var imagePickIndexPath : NSIndexPath?
    var generatorType: SWBaseFormGeneratorSourceType = .NONE
    
    var arrAlternateColors: Array<UIColor> = []
    //--------------------------------------
    // MARK: - Lazy Properties
    //--------------------------------------
    
    lazy var validator : SWValidator = SWValidator()
    
    lazy var dataSource : SWDataSource = SWDataSource()
    
    @IBOutlet weak var btnAction: UIBarButtonItem!
    weak public var formManager: SWFormManager?
    
    // MARK:- Init Methods
    class public func initWith(URL: String, stryboardName:String, viewControllerIdentifier:String) -> AnyObject? {
        let storyboard = UIStoryboard(name: stryboardName, bundle: NSBundle(forClass: SWBaseFormGenerator.self))
        if let controller = storyboard.instantiateViewControllerWithIdentifier(viewControllerIdentifier) as? SWBaseFormGenerator
        {
            controller.generatorType = SWBaseFormGeneratorSourceType.URL
            controller.formMetaDataURL = URL
            return controller
        }
        return nil
    }
    
    class public func initWithformMetaData(formMetaDataDict: Dictionary<String,AnyObject>, stryboardName:String, viewControllerIdentifier:String) -> AnyObject? {
        let storyboard = UIStoryboard(name: stryboardName, bundle: NSBundle(forClass: SWBaseFormGenerator.self))
        if let controller = storyboard.instantiateViewControllerWithIdentifier(viewControllerIdentifier) as? SWBaseFormGenerator
        {
            controller.generatorType = SWBaseFormGeneratorSourceType.JSON
            controller.formMetaData = formMetaDataDict
            return controller
        }
        return nil
    }
    class public func initWithLocalURL(URL: String, stryboardName:String, viewControllerIdentifier:String) -> AnyObject? {
        let storyboard = UIStoryboard(name: stryboardName, bundle: NSBundle(forClass: SWBaseFormGenerator.self))
        if let controller = storyboard.instantiateViewControllerWithIdentifier(viewControllerIdentifier) as? SWBaseFormGenerator
        {
            controller.generatorType = SWBaseFormGeneratorSourceType.LOCAL_FILE
            controller.formMetaDataURL = URL
            return controller
        }
        return nil
    }
    
    // MARK:- View Life Cycle Methods
    override public func viewDidLoad() {
        super.viewDidLoad()
        switch(self.generatorType)
        {
        case SWBaseFormGeneratorSourceType.NONE :
            print("")
        case SWBaseFormGeneratorSourceType.JSON :
            self.loadFormMetaData(self.formMetaData)
            print("")
        case SWBaseFormGeneratorSourceType.URL :
            print("")
            self.loadFormWith(self.formMetaDataURL)
        case SWBaseFormGeneratorSourceType.LOCAL_FILE :
            print("")
            self.loadFormWithLocalpath(self.formMetaDataURL)
        default:
            print("")
            
        }
    }
    override public func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    // MARK:- Convinience Methods
    func loadFormWith(URL: String)
    {
        if self.delegate != nil
        {
            self.delegate?.formGenerator!(self, startedLoadingURLWith: URL)
        }

        SWDataManager.sharedInstance.getJSONForFormId("123", withURL:URL) { (status: Bool, response: AnyObject?) -> () in
            if status
            {
                dispatch_async(dispatch_get_main_queue(),{
                    self.loadFormMetaData(response as! Dictionary)
                })
            }
            else
            {
                //Handle error
                self.delegate?.formGenerator!(self, doneJSONLoadWith: self.formMetaData)
                self.navigationController?.popToRootViewControllerAnimated(true)
            }

        }
        
        
       
    }
    func loadFormWithLocalpath(path: String)
    {
        if self.delegate != nil
        {
            self.delegate?.formGenerator!(self, startedLoadingURLWith: path)
        }
        
        if NSFileManager.defaultManager().fileExistsAtPath(path)
        {
            let err: NSError?
            let data : NSData = NSFileManager.defaultManager().contentsAtPath(path)!
            let _formMetaData = (try! NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)) as! Dictionary<String,AnyObject>
            if err != nil
            {
                self.loadFormMetaData(_formMetaData)
            }
            else
            {
                //Error
            }
        }
        else
        {
            //File does not exists
        }
        
    }
    
    func loadFormMetaData(dataDict: Dictionary<String,AnyObject>)
    {
        self.formMetaData = dataDict
        if self.delegate != nil && self.formMetaData != nil
        {
            self.delegate?.formGenerator!(self, startedLoadingJSONWith: self.formMetaData)
        }
        
        self.tableView.dataSource = self.dataSource
        
        if self.delegate != nil{
            self.delegate?.formGenerator!(self, doneJSONLoadWith: dataDict)
        }
        if self.delegate != nil && self.formMetaData != nil{
            
            self.delegate?.formGenerator!(self, startedFormRenderingWith: self.formMetaData)
            
            self.dataSource.reloadDataWith(self.formMetaData, withCellConfigureBlock: {
                (cell :UITableViewCell, indexPath:NSIndexPath) -> () in
                }, withTableView: self.tableView,_viewController: self)
        }
        
        if self.dataSource.formMetaData?.navigationInfo != nil{
            self.btnAction.title = self.dataSource[navigation: "button_title"]
        }
        
        if self.delegate != nil && self.formMetaData != nil{
            self.delegate?.formGenerator!(self, doneFormRenderingWith: self.formMetaData)
        }
        
    }
    
    //--------------------------------------
    // MARK: - UITableViewDelegate
    //--------------------------------------
    
    override public func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if let attrib = self.dataSource[indexPath.row]!.attributes_Info{
            let cellHeight = attrib["cell_height"] as! CGFloat
            return cellHeight
        }
        return 64
    }
    
    override public func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath)
        if let identifier = SWFormFieldType(rawValue: (tableView.cellForRowAtIndexPath(indexPath)!.reuseIdentifier)!){
            
            let objRBTVC = self.storyboard?.instantiateViewControllerWithIdentifier("SWOptionTableViewController") as! SWOptionTableViewController
            
            let _fieldInfo = self.dataSource.formMetaData!.fields[indexPath.row] //old
            
            let fieldInfo = self.dataSource[indexPath.row] //with subscripting
            
            objRBTVC.fieldInfo = fieldInfo
            
            let finalJSON = self.dataSource.composedResultDict
            
            objRBTVC.delegate = self
            
            switch identifier{
            case .RadioButton:
                objRBTVC.navTitle = "Choose your Option"
                objRBTVC.isCheckBoxType = false
                
            case .CheckBox:
                objRBTVC.navTitle = "Select Options"
                objRBTVC.isCheckBoxType = true
                
            default:
                print("ignore the did select", terminator: "")
                return;
            }
            self.navigationController?.pushViewController(objRBTVC, animated: true)
        }
    }
    
    override public func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if arrAlternateColors.count < 1 {
            return;
        }
        let index: Int = Int(indexPath.row % self.arrAlternateColors.count)
        let color = (arrAlternateColors[index]) as UIColor?
                cell.backgroundView?.backgroundColor = color
                cell.contentView.backgroundColor = color
    }
    
    //--------------------------------------
    // MARK: - Action Methods
    //--------------------------------------
    
    @IBAction func handleAction(sender: AnyObject) {
        
        let urlNavigate = self.dataSource.formMetaData?.getNavivationURL()
        
        //--------------------------------------
        // MARK: - ValidateData
        //--------------------------------------
        
        self.validator.validateForForm(self.dataSource.formMetaData, validatioCompletionBlock: { (status: Bool, msg:String?) -> () in
            if status{
                
                let action = SWFormAction(rawValue: self.dataSource[navigation: "button_title"]!)!

                switch action{
                case .None:
                    print("Do Nothing", terminator: "")
                    return
                    
                case .Next:
                    print("Do Next", terminator: "")
                    self.actionFormNext()

                case .Submit:
                    print("Do Nothing", terminator: "")
                    self.actionFormSubmit()
                    
                case .Done:
                    print("Do Done", terminator: "")
                    self.actionFormDone()

                case .Cancel:
                    print("Do Cancel", terminator: "")
                    self.actionFormCancel()

                case .Previous:
                    print("Do Previous", terminator: "")
                    self.actionFormPrev()
                    
                }
            }
        })
    }
    
    
    //MARK:- SWButtonCellDelegte methods
    func buttonClickedFor(cell: SWButtonCell, with fieldData : SWFieldVO)
    {
        if self.delegate != nil
        {
            self.delegate?.formGenerator!(self, buttonTappedWith: cell.button, withFieldId: fieldData.id)
        }
    }
    //MARK:- SWImagePickerCellDelegate methods
    func imagePickDidSelectedFor(cell: SWImagePickerCell, fieldData: SWFieldVO, withCompletion imagePickCompletionBlock:SWImagePickCompletionBlock )
    {
        self.imagePickIndexPath = self.tableView.indexPathForCell(cell)!
        if self.imagePicker == nil
        {
            self.imagePicker = UIImagePickerController()
            self.imagePicker?.delegate = self
        }
        self.imagePicker!.allowsEditing = false
        self.imagePicker!.sourceType = .PhotoLibrary
        self.imagePicCompletionBlock = imagePickCompletionBlock
        
        presentViewController(self.imagePicker!, animated: true, completion: nil)
    }
    
    //MARK:- SWSegmentCellDelegate methods
    func segmentDidChangedFor(cell: SWSegmentCell, selectedIndex: Int,fieldData : SWFieldVO)
    {
        cell.fieldInfo.resultDict = [ cell.fieldInfo.fieldKey : "\(selectedIndex)"]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    //MARK:- SWSliderCellDelegate methods
    func sliderDidChangedFor(cell: SWSliderCell, range: Float,fieldData : SWFieldVO)
    {
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : "\(range)"]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
        
        //        self.dataSource[cell.fieldInfo.fieldKey:"\(range)" ]
        
    }
    
    //MARK:- SWStepperCellDelegate methods
    func steppeDidChangedFor(cell: SWStepperCell, range: Double,fieldData : SWFieldVO)
    {
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : "\(range)"]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    //MARK:- SWSwitchCellDelegate methods
    func switchDidChangedFor(cell: SWSwitchCell, state: UIControlState,fieldData : SWFieldVO)
    {
        let _state = state.rawValue
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : "\(_state)"]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    //MARK:- SWOptionTableViewControllerDelegate methods
    
    func didSelectedOption(selectedOption: Dictionary<String, String>, with fieldInfo : SWFieldVO){
        
        
    }
    func didSelectedOptionsWith(selectedOptions:[Dictionary<String, String>], with fieldInfo: SWFieldVO){
        
    }

    func _didSelectedOptionsWith(selectedOptions:[String], with fieldInfo: SWFieldVO)
    {
        let indexPath = tableView.indexPathForSelectedRow
        let cell = tableView.cellForRowAtIndexPath(indexPath!) as! SWCheckBoxCell
        
        let stringRepresentation = selectedOptions.joinWithSeparator(", ")
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : selectedOptions]
        cell.seleckedOptions.text = stringRepresentation
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
        print(selectedOptions)
    }
    
    func _didSelectedOption(selectedOption: String, with fieldInfo : SWFieldVO)
    {
        
        let indexPath = tableView.indexPathForSelectedRow
        let cell = tableView.cellForRowAtIndexPath(indexPath!) as! SWRadioButtonCell
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : [selectedOption]]
        cell.slectedOption.text = selectedOption
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
        print(selectedOption)
    }
    
    
    //MARK:- SWTextFieldCellDataChangeDelegate delegates
    func textFieldCell(cell: SWTextFieldCell, dataChangedWith data: String)
    {
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey : data ]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    
    
    
    //MARK:- SWTextViewCellDataChangeDelegate delegates
    func textViewCell(cell: SWTextViewCell, dataChangedWith data: String)
    {
        cell.fieldInfo.resultDict = [cell.fieldInfo.fieldKey :  data]
        self.dataSource.composedResultDict.removeValueForKey(cell.fieldInfo.id)
        self.dataSource.composedResultDict[cell.fieldInfo.id] = cell.fieldInfo.resultDict!
    }
    
    // MARK:- UIImagePickerControllerDelegate Methods
    
    public func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let cell = self.tableView.cellForRowAtIndexPath(self.imagePickIndexPath!) as? SWImagePickerCell
            cell!.fieldInfo.resultDict = [cell!.fieldInfo.fieldKey : pickedImage]
            self.dataSource.composedResultDict.removeValueForKey(cell!.fieldInfo.id)
            self.dataSource.composedResultDict[cell!.fieldInfo.id] = cell!.fieldInfo.resultDict!
            self.imagePicCompletionBlock!(pickedImage)
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    //--------------------------------------
    // MARK: - Convenience
    //--------------------------------------

    func setAlternatingColors(hexColors: String?...){
        self.arrAlternateColors.removeAll(keepCapacity: false)
        if hexColors.count>0{
            for color in hexColors{
                self.arrAlternateColors.append(UIColor.colorFromHex(color!))
            }
        }else{
            self.arrAlternateColors.append(UIColor.whiteColor())
        }
    }
}
