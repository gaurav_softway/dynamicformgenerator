//
//  SWBaseFormGenerator+Actions.swift
//  DynamicFormGenerator
//
//  Created by Gaurav Keshre on 7/2/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
import UIKit

enum SWActionState{
    case BeforeAction
    case OnAction
    case AfterAction
    case AskBeforeAction
}
extension SWBaseFormGenerator{
    
    //--------------------------------------
    // MARK: - Form NavigationAction
    //--------------------------------------
    /*
    if (self.delegate != nil)
    {
    var shouldSubmit = self.delegate?.formGenerator(self, shouldSubmitJSON: finalJSON, withURL: URL!)
    if (shouldSubmit == false)
    {
    shouldnavigateNextOrPrev = false
    }
    else
    {
    self.delegate?.formGenerator(self, willSubmitJSON: finalJSON, withURL: URL!)
    //                                NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
    //                                    var _formMetaData = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &err) as! NSDictionary
    //                                    dispatch_async(dispatch_get_main_queue(),{
    //                                if (self.delegate != nil)
    //                                {
    //                                    self.delegate?.formGenerator(self, didSubmitJSONwith: _formMetaData, withURL: urlString)
    //                                    check = false
    //                                }
    //                                    })
    //                                });
    
    }
    }
    
    */
    func actionFormDone(){
        
    }

    func actionFormSubmit(){
        
    }
    
    func actionFormCancel(){
        
    }
    
    func actionFormNext(){
        
    }
    
    func actionFormPrev(){
        
    }
    
    //--------------------------------------
    // MARK: - Delegation
    //--------------------------------------
    
    func informDelegateOfFormAction(action: SWFormAction, on:SWActionState, withObject object: AnyObject?)-> Bool?{
        
        if self.delegate == nil {
            return false
        }

//        if let delegate = self.delegate{
//            
//        }
        return false
    }

}
