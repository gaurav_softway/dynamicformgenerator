//
//  SWCheckBoxCollectionCell.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 18/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import UIKit

class SWCheckBoxCollectionCell: UICollectionViewCell {
    var cellInfo = Dictionary<String, String>()
    @IBOutlet weak var lblOptionText: UILabel!
}
