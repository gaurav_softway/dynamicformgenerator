//
//  SWFormVO.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 18/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
//MARK:- SWFormVO
class SWFormVO: SWBaseVO {
//    var sectons : [SWSectionVO] = []
    var fields : [SWFieldVO] = []
    let navigationInfo : Dictionary<String,AnyObject>?
    
    init(jsonData:Dictionary<String,AnyObject>)
    {
        self.navigationInfo = (jsonData["navigation_info"] as? Dictionary<String,AnyObject>) ?? nil
        
        super.init(_id: jsonData["form_id"] as! String, _fallBackInfo: ((jsonData["fallback_info"] as? Dictionary<String,AnyObject>) ?? nil)!)
//        let arrSections : [AnyObject] = (jsonData["sections"] as? Array)!
//        for dataDict in arrSections
//        {
//            var section :SWSectionVO = SWSectionVO(sectionData: dataDict as! NSDictionary)
//            self.sectons.append(section)
//        }
        let arrFields : [AnyObject] = (jsonData["fields"] as? Array)!
        for dataDict in arrFields
        {
            let formField :SWFieldVO = SWFieldVO(fieldData: dataDict as! Dictionary<String,AnyObject>)
            self.fields.append(formField)
        }
    }
    func createFallbackWith(baseURL: String) -> String
    {
        let URLString = self.navigationInfo?["url"] as! String
        return baseURL.stringByAppendingString(URLString).stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
    }

    //TODO:- Need to complete this implementation
    func getNavivationURL() -> String{
        return self.navigationInfo?["url"] as! String
    }

}
