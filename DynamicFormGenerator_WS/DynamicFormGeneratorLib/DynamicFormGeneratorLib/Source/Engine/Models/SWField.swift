//
//  SWFieldVO.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 18/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
import UIKit
//MARK:- SWFieldVO
class SWFieldVO: SWBaseVO {
    let caption : String
    let fieldType : SWFormFieldType
    let required : Bool
    let enabled : Bool
    let order : Int
    var options : [Dictionary<String, String>]
    let defaultOptionId : String?
    let placeholder : String?
    let keyboard_type : UIKeyboardType? // UIKeyboardType
    let default_Value : String?
    var needsValidation: Bool
    var validation_Info : Dictionary<String,AnyObject>?
    var attributes_Info : Dictionary<String,AnyObject>?
    let fieldKey : String
    var fallBack : String?
    var resultDict: Dictionary<String,AnyObject>?
    
    init(fieldData:Dictionary<String,AnyObject>)
    {
        self.caption = fieldData["caption"] as! String
        self.required = fieldData["required"] as! Bool
        self.fieldType = enumFromStringFieldType(fieldData["type"] as! String)
        self.enabled = fieldData["enabled"] as! Bool
        self.order = fieldData["order"] as! Int
        self.fieldKey = fieldData["key"] as? String ?? ""
        self.placeholder = fieldData["placeholder"] as? String ?? ""
        if fieldData["keyboard_type"] != nil
        {
            self.keyboard_type = UIKeyboardType(rawValue: (fieldData["keyboard_type"] as? Int)!)
        }
        else
        {
            self.keyboard_type = UIKeyboardType.Default
        }
        self.default_Value = (fieldData["default_value"] as? String) ?? ""
        self.validation_Info = (fieldData["validation"] as? Dictionary<String,AnyObject>) ?? nil
        self.needsValidation = false
        if let validI = self.validation_Info{
            self.needsValidation = (self.validation_Info?["validation_required"] as! Bool)
        }
        self.attributes_Info = (fieldData["attributes"] as? Dictionary<String,AnyObject>) ?? nil
        self.options = (fieldData["options"] as? Array) ?? [Dictionary<String, String>]()
        self.defaultOptionId = (fieldData["default_option"] as? String) ?? ""
        let strId : String = fieldData["field_id"] as! String
        self.fallBack = fieldData["fallback"] as? String ?? ""
        super.init(_id: strId)
    }
    
    //--------------------------------------
    // MARK: - Key Substring
    //--------------------------------------
    
    subscript (attribute key: String) -> AnyObject? {
        if let k: AnyObject = self.attributes_Info![key]{
            return k
        }
        return nil
    }


}