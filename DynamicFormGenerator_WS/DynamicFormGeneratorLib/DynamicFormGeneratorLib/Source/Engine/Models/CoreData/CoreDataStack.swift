//
//  CoreDataStack.swift
//  GK
//
//  Created by Gaurav Keshre on 6/9/15.
//  Copyright (c) 2015 Gaurav Keshre. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack: AnyObject {
    var modelName: String
    
    init(modelName xcdataModel: String){
      modelName = xcdataModel as String
    }

    lazy var managedObjectContext: NSManagedObjectContext = {
        var _moc = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        _moc.persistentStoreCoordinator = self.persistentStoreCoordinator;
        return _moc
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        print(self.modelName)
        
        let bundlePath = NSBundle(identifier: "SS.DynamicFormGeneratorLib")
        
        let modelURL = bundlePath!.URLForResource(self.modelName, withExtension: "momd")
        print(modelURL)

        return NSManagedObjectModel(contentsOfURL: modelURL!)!
        }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        var psc:NSPersistentStoreCoordinator? =
            NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        
        var error: NSError? = nil;
        
        do {
            try psc!.addPersistentStoreWithType(NSSQLiteStoreType,
                        configuration: nil,
                        URL: self.storeURL,
                        options: nil)
        } catch var error1 as NSError {
            error = error1
                psc = nil;
                //WARNING:
                // there was an error loadint the store.
                // this mezans there is some problem.
            } catch {
            fatalError()
        }
        return psc!;
        
    }()
    //--------------------------------------
    // MARK: - SAVE
    //--------------------------------------
    // MARK: - Core Data Saving support
    
    func saveContext () {
            var error: NSError? = nil
            if self.managedObjectContext.hasChanges {
                do {
                    try self.managedObjectContext.save()
                } catch let error1 as NSError {
                    error = error1
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog("Unresolved error \(error), \(error!.userInfo)")
                    //abort()
                }
            }
    }


    //--------------------------------------
    // MARK: - Convenience
    //--------------------------------------
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "research.softway.sync.coredata_sample" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] 
        }()

    //--------------------------------------
    // MARK: - URLs
    //--------------------------------------
    lazy var storeURL: NSURL = {
        return (self.applicationDocumentsDirectory).URLByAppendingPathComponent("\(self.modelName).sqlite") as NSURL
        }()

}

