//
//  SWFormMO.swift
//  DynamicFormGenerator
//
//  Created by Sugeet on 06/07/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
import CoreData
@objc(SWFormMO)

class SWFormMO: NSManagedObject {

    @NSManaged var id: String
    @NSManaged var last_modified_date: NSDate
    @NSManaged var path: String
    @NSManaged var checksum: String
   
    //MARK: - Insert SWFormMO Data
    
    class func insertFormDataWith(formID: String?, FormPath formPath:String?, AndLastModifiedDate lastModfiedDate: NSDate?, inManagedObjectContext moContext: NSManagedObjectContext?) {
        
        let form = SWFormMO.insertInManagedObjectContext(moContext)
        form.id = formID!
        form.last_modified_date = lastModfiedDate!
        form.checksum = "Check Sum"
        form.path = formPath!
    }
    
    class func insertInManagedObjectContext(moContext: NSManagedObjectContext?) -> SWFormMO  {
        let form =  NSEntityDescription.insertNewObjectForEntityForName("SWFormMO", inManagedObjectContext: moContext!) as! SWFormMO
        return form
    }
    
    //MARK: - Delete SWFormMO Data
    
    class func deleteFormDataForId(formId: String) {
        CoreDataManager.deleteAllObjectsForEntity("SWFormMO", withPredicate: NSPredicate(format: "id ==[c] %@", formId), andContext: CoreDataManager.mainManagedObjectContext())
    }
    
    //MARK: - Fetch SWFormMO Data
    class func getFormData(formId: String) -> NSArray? {
        let predicate = NSPredicate(format: "id ==[c] %@", formId)
        let formData = CoreDataManager.searchObjectsForEntity("SWFormMO", withPredicate: predicate, andSortKey: nil, andSortAscending: true)
        return formData
    }

}
