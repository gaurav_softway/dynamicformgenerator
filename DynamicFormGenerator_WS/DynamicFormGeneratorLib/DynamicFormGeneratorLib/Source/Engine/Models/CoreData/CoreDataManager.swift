//
//  CoreDataManager.swift
//  DynamicFormGenerator
//
//  Created by Sugeet on 03/07/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.


import Foundation
import CoreData

class CoreDataManager: CoreDataStack {
    
    class var sharedInstance : CoreDataManager {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : CoreDataManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = CoreDataManager(modelName: "SWFormGeneratorModel")
        }
        return Static.instance!
    }
    
    //--------------------------------------
    // MARK: - Managed Object Context
    //--------------------------------------
    
    class func mainManagedObjectContext() -> NSManagedObjectContext{
        return CoreDataManager.sharedInstance.managedObjectContext
    }

    //--------------------------------------
    // MARK: - Get Data
    //--------------------------------------
    
    class func getObjectsForEntity(entityName: String?, withSortKey sortKey: String?, andSortAscending sortAscending: Bool?) -> NSArray {
        return CoreDataManager.getObjectsForEntity(entityName, withSortKey: sortKey, andSortAscending: sortAscending, andContext: CoreDataManager.mainManagedObjectContext())
    }
    
    class func getObjectsForEntity(entityName: String?, withSortKey sortKey: String?, andSortAscending sortAscending: Bool?, andContext managedObjectContext : NSManagedObjectContext?) -> NSArray {
            return CoreDataManager.searchObjectsForEntity(entityName, withPredicate: nil, andSortKey: sortKey, andSortAscending: sortAscending, andContext: managedObjectContext!)
    }
    
     class func searchObjectsForEntity(entityName: String?, withPredicate predicate:NSPredicate?, andSortKey sortKey: String?, andSortAscending sortAscending: Bool?) -> NSArray {
    
        return CoreDataManager.searchObjectsForEntity(entityName, withPredicate: predicate, andSortKey: nil, andSortAscending: sortAscending, andContext: CoreDataManager.mainManagedObjectContext())
        
    }
    
    //--------------------------------------
    // MARK: - Search Data
    //--------------------------------------
    
    class func searchObjectsForEntity(entityName: String?, withPredicate predicate:NSPredicate?, andSortKey sortKey: String?, andSortAscending sortAscending: Bool?, andContext managedObjectContext: NSManagedObjectContext?) -> NSArray {
       
        // Create fetch request
        var request = NSFetchRequest() as NSFetchRequest?
        let entity = NSEntityDescription.entityForName(entityName!, inManagedObjectContext: managedObjectContext!)
        request!.entity = entity
        
        // If a predicate was specified then use it in the request
        if predicate != nil {
            request!.predicate = predicate
        }

        // If a sort key was passed then use it in the request
        if(sortKey != nil) {
            let sortDescriptor = NSSortDescriptor(key: sortKey!, ascending: sortAscending!)
            var sortDescriptors = NSArray(objects: sortDescriptor)
        }
        
        // Execute the fetch request
        
        var error:NSError?
        
        var mutableFetchResults: [AnyObject]?
        do {
            mutableFetchResults = try managedObjectContext!.executeFetchRequest(request!)
        } catch let error1 as NSError {
            error = error1
            mutableFetchResults = nil
        }
        request = nil
        
        // If the returned array was nil then there was an error
        if mutableFetchResults == nil {
            print("Couldn't get objects for entity /entityName")
        }
        return (mutableFetchResults! as AnyObject!) as! NSArray
    }
    
    //--------------------------------------
    // MARK: - Delete Data
    //--------------------------------------
    
    class func deleteAllObjectsForEntity(entityName: String?) -> Bool {
        return CoreDataManager.deleteAllObjectsForEntity(entityName, andContext: CoreDataManager.mainManagedObjectContext())
    }
    
    class func deleteAllObjectsForEntity(entityName: String?, andContext moc: NSManagedObjectContext?) -> Bool{
        return CoreDataManager.deleteAllObjectsForEntity(entityName, withPredicate: nil, andContext: moc)
    }
    
    class func deleteAllObjectsForEntity(entityName: String?, withPredicate predicate: NSPredicate?,  andContext moc: NSManagedObjectContext?) -> Bool{
        
        // Create fetch request
        var request = NSFetchRequest() as NSFetchRequest?
        let entity = NSEntityDescription.entityForName(entityName!, inManagedObjectContext: moc!)
        request!.entity = entity
        
        // Ignore property values for maximum performance
        request?.includesPropertyValues = false
        
        // If a predicate was specified then use it in the request
        if (predicate != nil) {
            request?.predicate = predicate
        }
        
        // Execute the count request
        var error:NSError?
        let fetchResults = moc!.executeFetchRequest(request!) as? [NSManagedObject]
        
        request = nil
        
        // Delete the objects returned if the results weren't nil
        if (fetchResults != nil) {
            for mainObj in fetchResults! {
                moc!.deleteObject(mainObj)
            }
        } else {
            print("Couldn't delete objects for entity /entityName")
            return false
        }
        return false;
    }
    
    //--------------------------------------
    // MARK: - Save Data
    //--------------------------------------
    
    class func saveWithError() throws {
        var errorPtr: NSError! = NSError(domain: "Migrator", code: 0, userInfo: nil)
        var flag: Bool
        do {
            try self.mainManagedObjectContext().save()
            flag = true
        } catch let error as NSError {
            errorPtr = error
            flag = false
        }
        if flag && (false) {
            return
        }
        throw errorPtr
    }

}