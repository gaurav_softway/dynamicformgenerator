//
//  SWSectionVO.swift
//  DynamicFormGenerator
//
//  Created by Hemanth on 18/06/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//

import Foundation
//MARK:- SWSectionVO
class SWSectionVO: SWBaseVO {
    var fields : [SWFieldVO] = []
    let titleInfo : Dictionary<String,AnyObject>?
    let actionInfo : Dictionary<String,AnyObject>?
    
    
    init(sectionData:NSDictionary)
    {
        //println("section data = \(sectionData)")
        self.titleInfo = (sectionData["title_info"] as? Dictionary<String,AnyObject>) ?? nil
        self.actionInfo = (sectionData["actions"] as? Dictionary<String,AnyObject>) ?? nil
        let strId = sectionData["section_id"] as! String
        super.init(_id: strId)
        let arrFields : [AnyObject] = (sectionData["fields"] as? Array)!
        for dataDict in arrFields
        {
            let formField :SWFieldVO = SWFieldVO(fieldData: dataDict as! Dictionary<String,AnyObject>)
            self.fields.append(formField)
        }
    }
}