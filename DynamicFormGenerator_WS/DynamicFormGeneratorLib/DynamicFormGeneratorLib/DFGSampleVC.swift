//
//  DFGSampleVC.swift
//  DynamicFormGeneratorLib
//
//  Created by Sugeet on 14/09/15.
//  Copyright (c) 2015 Softway Solutions. All rights reserved.
//
import Foundation
import UIKit

public class DFGSampleVC {
 
    public class var sharedInstance: DFGSampleVC {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: DFGSampleVC? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance  = DFGSampleVC()
        }
        return Static.instance!
    }
    
    public func sampleMethod() {
        
        print("I am a sample method from Framework!!!!!!")
    }
}
